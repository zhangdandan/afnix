# ----------------------------------------------------------------------------
# - std-axp-print                                                            -
# - afnix:std:axp printer unit                                               -
# ----------------------------------------------------------------------------
# - This program is  free software;  you can  redistribute it and/or  modify -
# - it provided that this copyright notice is kept intact.                   -
# -                                                                          -
# - This  program  is  distributed in the hope  that it  will be useful, but -
# - without  any   warranty;  without  even   the   implied    warranty   of -
# - merchantability  or fitness for a particular purpose. In not event shall -
# - the copyright holder be  liable for  any direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.      -
# ----------------------------------------------------------------------------
# - copyright (c) 1999-2023 amaury darsch                                    -
# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# - global section                                                           -
# ----------------------------------------------------------------------------

# the option database class
const afnix:std:axp:print (class)
# the print nameset
try (const AFNIX:STD:AXP:PRINT (nameset AFNIX:STD:AXP))

# ----------------------------------------------------------------------------
# - private section                                                          -
# ----------------------------------------------------------------------------

# the option messages
const AFNIX:STD:AXP:PRINT:U-CLS-MSG "axi [i afnix-std-axp cmd-std-print] [-]"
const AFNIX:STD:AXP:PRINT:H-LCO-MSG "    [h]      print this help message"
const AFNIX:STD:AXP:PRINT:V-LCO-MSG "    [v]      print system version"
const AFNIX:STD:AXP:PRINT:P-LCO-MSG "    [p]      parse only the source content"
const AFNIX:STD:AXP:PRINT:X-LCO-MSG "    [x]      set output file extension"
const AFNIX:STD:AXP:PRINT:O-LCO-MSG "    [o]      set output file name"

# ----------------------------------------------------------------------------
# - initial section                                                          -
# ----------------------------------------------------------------------------

# preset the printer class
# @param argv the argument vector
trans afnix:std:axp:print:preset (argv) {
  # preini the print class
  this:preini argv
  # postdo the print class
  this:postdo
}

# initialize the option descriptors
trans afnix:std:axp:print:preini (argv) {
  # create an option class and bind it
  trans this:super (afnix:sys:Options AFNIX:STD:AXP:PRINT:U-CLS-MSG)

  # register the options
  this:add-string-option 'o' AFNIX:STD:AXP:PRINT:O-LCO-MSG
  this:add-string-option 'x' AFNIX:STD:AXP:PRINT:X-LCO-MSG
  this:add-unique-option 'p' AFNIX:STD:AXP:PRINT:P-LCO-MSG
  this:add-unique-option 'v' AFNIX:STD:AXP:PRINT:V-LCO-MSG
  this:add-unique-option 'h' AFNIX:STD:AXP:PRINT:H-LCO-MSG

  # parse the options
  try (this:parse argv) {
    this:usage (interp:get-error-stream)
    afnix:sys:exit 1
  }
  # check for the help option
  if (this:get-unique-option 'h') {
    this:usage (interp:get-output-stream)
    afnix:sys:exit 0
  }
  # check for the version option
  if (this:get-unique-option 'v') {
    println (afnix:std:axp:get-copyright-message)
    println (afnix:std:axp:get-revision-message)
    afnix:sys:exit 0
  }
  # get the requested uri argument
  const varg (this:get-vector-arguments)
  if (!= (varg:length) 1) {
    this:usage (interp:get-error-stream)
    afnix:sys:exit 1
  }
  const this:suri (varg:get 0)
  # check for extension file name
  if (this:get-unique-option 'x') {
    # get the extension option
    trans afnix:std:axp:system-xnam (this:get-string-option 'x')
    # remove input extension
    trans afnix:std:axp:system-onam (afnix:sio:remove-extension this:suri)
    # add output extension
    afnix:std:axp:system-onam:+= '.'
    afnix:std:axp:system-onam:+= afnix:std:axp:system-xnam
  }
  # check for the output file name
  if (this:get-unique-option 'o') {
    trans afnix:std:axp:system-oflg true
    trans afnix:std:axp:system-onam (this:get-string-option 'o')
  }
}

# postdo the print command
trans afnix:std:axp:print:postdo nil {
  # check for present mode
  const this:pmod (this:get-unique-option 'p')
}

# ----------------------------------------------------------------------------
# - methods section                                                          -
# ----------------------------------------------------------------------------

# execute the command
trans afnix:std:axp:print:run nil {
  # check for parse mode
  if this:pmod {
    try {
      # create a new context
      const ctx (afnix:std:axp:ctxdb)
      # get the uri name from the arguments
      const uri (afnix:nwg:system-uri-name this:suri)
      # process source content by uri
      ctx:process-source-content uri
    } {
      errorln "error: " what:about
      afnix:sys:exit 1
    }
  }

  # process the content
  try {
    # create a new context
    const ctx (afnix:std:axp:ctxdb)
    # get the uri name from the arguments
    const uri (afnix:nwg:system-uri-name this:suri)
    # process source content by uri
    ctx:process uri
  } {
    errorln "error: " what:about
    afnix:sys:exit 1
  }
}
