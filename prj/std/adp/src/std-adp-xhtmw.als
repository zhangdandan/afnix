# ----------------------------------------------------------------------------
# - std-adp-xhtmw                                                            -
# - afnix:std:adp xhtml module writer                                        -
# ----------------------------------------------------------------------------
# - This program is  free software;  you can  redistribute it and/or  modify -
# - it provided that this copyright notice is kept intact.                   -
# -                                                                          -
# - This  program  is  distributed in the hope  that it  will be useful, but -
# - without  any   warranty;  without  even   the   implied    warranty   of -
# - merchantability  or fitness for a particular purpose. In not event shall -
# - the copyright holder be  liable for  any direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.      -
# ----------------------------------------------------------------------------
# - copyright (c) 1999-2023 amaury darsch                                    -
# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# - modules section                                                          -
# ----------------------------------------------------------------------------

interp:load "std-adp-xhtut"
interp:load "std-adp-xhtpg"
interp:load "std-adp-xhtwr"
