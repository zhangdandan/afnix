# ----------------------------------------------------------------------------
# - std-sps-param                                                            -
# - afnix:std:sps parameters definition unit                                 -
# ----------------------------------------------------------------------------
# - This program is  free software;  you can  redistribute it and/or  modify -
# - it provided that this copyright notice is kept intact.                   -
# -                                                                          -
# - This  program  is  distributed in the hope  that it  will be useful, but -
# - without  any   warranty;  without  even   the   implied    warranty   of -
# - merchantability  or fitness for a particular purpose. In not event shall -
# - the copyright holder be  liable for  any direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.      -
# ----------------------------------------------------------------------------
# - copyright (c) 1999-2023 amaury darsch                                    -
# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# - project section                                                          -
# ----------------------------------------------------------------------------

# system revision
const AFNIX:STD:SPS:MODULE-RMAJ 3
const AFNIX:STD:SPS:MODULE-RMIN 8
const AFNIX:STD:SPS:MODULE-PTCH 0

# the application name
const AFNIX:STD:SPS:MODULE-NAME "sps"
# the application info
const AFNIX:STD:SPS:MODULE-INFO "afnix sps command layer"
# the copyright holder
const AFNIX:STD:SPS:MODULE-COPY "© 1999-2023 by Amaury Darsch"
