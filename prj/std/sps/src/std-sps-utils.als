# ----------------------------------------------------------------------------
# - std-sps-utils                                                            -
# - afnix:std:sps utility fuspsions unit                                     -
# ----------------------------------------------------------------------------
# - This program is  free software;  you can  redistribute it and/or  modify -
# - it provided that this copyright notice is kept intact.                   -
# -                                                                          -
# - This  program  is  distributed in the hope  that it  will be useful, but -
# - without  any   warranty;  without  even   the   implied    warranty   of -
# - merchantability  or fitness for a particular purpose. In not event shall -
# - the copyright holder be  liable for  any direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.      -
# ----------------------------------------------------------------------------
# - copyright (c) 1999-2023 amaury darsch                                    -
# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# - copyright section                                                        -
# ----------------------------------------------------------------------------

# @return a formated revision string
const afnix:std:sps:get-revision-string nil {
  const rmaj AFNIX:STD:SPS:MODULE-RMAJ
  const rmin AFNIX:STD:SPS:MODULE-RMIN
  const ptch AFNIX:STD:SPS:MODULE-PTCH
  afnix:std:acl:get-revision-number rmaj rmin ptch
}

# @return the copyright message
const afnix:std:sps:get-copyright-message nil {
  # format the copyright message
  + (+ AFNIX:STD:SPS:MODULE-INFO ", ") AFNIX:STD:SPS:MODULE-COPY
}

# @return the full system version
const afnix:std:sps:get-revision-message nil {
  # get the revision info
  const mrev (afnix:std:sps:get-revision-string)
  # format the revision message
  const mesg (+ "revision " mrev)
  # add system info
  mesg:+= (+ ", afnix " interp:version) 
  mesg:+= (+ ", "       interp:os-name)
  # here it is
  eval mesg
}
