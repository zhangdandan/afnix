# ----------------------------------------------------------------------------
# - std-tls-inets                                                            -
# - afnix:std:tls inet server class module                                   -
# ----------------------------------------------------------------------------
# - This program is  free software;  you can  redistribute it and/or  modify -
# - it provided that this copyright notice is kept intact.                   -
# -                                                                          -
# - This  program  is  distributed in the hope  that it  will be useful, but -
# - without  any   warranty;  without  even   the   implied    warranty   of -
# - merchantability  or fitness for a particular purpose. In not event shall -
# - the copyright holder be  liable for  any direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.      -
# ----------------------------------------------------------------------------
# - copyright (c) 1999-2023 amaury darsch                                    -
# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# - global section                                                           -
# ----------------------------------------------------------------------------

# the inet server class
const afnix:std:tls:inets (class)
# the inet server nameset
try (const AFNIX:STD:TLS:INETS (nameset AFNIX:STD:TLS))

# ----------------------------------------------------------------------------
# - private section                                                          -
# ----------------------------------------------------------------------------

# the option messages
const AFNIX:STD:TLS:INETS:U-CLS-MSG "axi [i afnix-std-tls cmd-tls-inets] [-]"
const AFNIX:STD:TLS:INETS:H-LCO-MSG "    [h]      print this help message"
const AFNIX:STD:TLS:INETS:V-LCO-MSG "    [v]      print system version"
const AFNIX:STD:TLS:INETS:T-LCO-MSG "    [t]      connect with tcp socket"
const AFNIX:STD:TLS:INETS:P-LCO-MSG "    [p]      show the tls parameters"
const AFNIX:STD:TLS:INETS:D-LCO-MSG "    [d]      set the debug flag"
const AFNIX:STD:TLS:INETS:H-UCO-MSG "    [H]      set the host address"
const AFNIX:STD:TLS:INETS:P-UCO-MSG "    [P]      set the host port"
const AFNIX:STD:TLS:INETS:C-UCO-MSG "    [C path] set the certificate list"
const AFNIX:STD:TLS:INETS:K-UCO-MSG "    [K path] set the certificate key"
const AFNIX:STD:TLS:INETS:D-UCO-MSG "    [D path] set the dhe parameters"

# ----------------------------------------------------------------------------
# - initial section                                                          -
# ----------------------------------------------------------------------------

# preset the inet server class
# @param argv the argument vector
trans afnix:std:tls:inets:preset (argv) {
  # preini the server class
  this:preini argv
  # postdo the server class
  this:postdo
}

# preini the inet server class
# @param argv the argument vector
trans afnix:std:tls:inets:preini (argv) {
  # create an option class and bind it
  const this:opts (afnix:sys:Options AFNIX:STD:TLS:INETS:U-CLS-MSG)
  # register the options
  this:opts:add-string-option 'D' AFNIX:STD:TLS:INETS:D-UCO-MSG
  this:opts:add-string-option 'K' AFNIX:STD:TLS:INETS:K-UCO-MSG
  this:opts:add-string-option 'C' AFNIX:STD:TLS:INETS:C-UCO-MSG
  this:opts:add-string-option 'P' AFNIX:STD:TLS:INETS:P-UCO-MSG
  this:opts:add-string-option 'H' AFNIX:STD:TLS:INETS:H-UCO-MSG
  this:opts:add-unique-option 'd' AFNIX:STD:TLS:INETS:D-LCO-MSG
  this:opts:add-unique-option 'p' AFNIX:STD:TLS:INETS:P-LCO-MSG
  this:opts:add-unique-option 't' AFNIX:STD:TLS:INETS:T-LCO-MSG
  this:opts:add-unique-option 'v' AFNIX:STD:TLS:INETS:V-LCO-MSG
  this:opts:add-unique-option 'h' AFNIX:STD:TLS:INETS:H-LCO-MSG
  # parse the options
  try (this:opts:parse argv) {
    this:opts:usage (interp:get-error-stream)
    afnix:sys:exit 1
  }
  # check for the help option
  if (this:opts:get-unique-option 'h') {
    this:opts:usage (interp:get-output-stream)
    afnix:sys:exit 0
  }
  # check for the version option
  if (this:opts:get-unique-option 'v') {
    println (afnix:std:tls:get-copyright-message)
    println (afnix:std:tls:get-revision-message)
    afnix:sys:exit 0
  }
}

# postdo the inet server class
trans afnix:std:tls:inets:postdo nil {
  # create a tcp server socket
  if (this:opts:get-unique-option 'H') {
    const this:host (this:opts:get-string-option 'H')
  } (const this:host AFNIX:STD:TLS:SERVER-HOST)
  if (this:opts:get-unique-option 'P') {
    const this:port (Integer (this:opts:get-string-option 'P'))
  } (const this:port AFNIX:STD:TLS:SERVER-PORT)
  # get options
  const this:dbug (this:opts:get-unique-option 'd')
  const this:topt (this:opts:get-unique-option 't')
  # create the tls parameters
  const this:prms (afnix:tls:TlsParams this:host this:port)
  const this:popt (this:opts:get-unique-option 'p')
  # check for a certificate list
  if (this:opts:get-unique-option 'C') {
    const cert (this:opts:get-string-option 'C')
    this:prms:set-certificate cert
  }
  # check for a certificate key
  if (this:opts:get-unique-option 'K') {
    const ckey (this:opts:get-string-option 'K')
    this:prms:set-certificate-key ckey
  }
  # check for a dhe parameters
  if (this:opts:get-unique-option 'D') {
    const dhep (this:opts:get-string-option 'D')
    this:prms:set-dhe-parameters dhep
  }
}

# execute the command
trans afnix:std:tls:inets:run nil {
  # show the tls parameters
  if this:popt (this:show-tls-params)
  # connect the tls socket and get the connected socket
  const cs (if this:topt (this:accept-tcp-socket) (this:accept-tls-socket))
  if (nil-p cs) (return)
  # report in debug mode
  if this:dbug (afnix:std:tls:write-error-plist (cs:get-info))
  # collect the input/output stream
  const is (cs:get-input-stream)
  if (nil-p is) {
    errorln "[inetc] nil input stream in connect"
    afnix:sys:exit 1
  }
  const os (cs:get-output-stream)
  if (nil-p os) {
    errorln "[inetc] nil output stream in connect"
    afnix:sys:exit 1
  }
  # launch the read/write thread
  launch (this:read-loop is)
  launch (this:write-loop os)
}

# ----------------------------------------------------------------------------
# - report section                                                          -
# ----------------------------------------------------------------------------

trans afnix:std:tls:inets:show-tls-params nil {
  # get the cipher suite table
  const ptbl (this:prms:get-info)
  # format the table
  afnix:std:tls:write-error-plist ptbl
}

# ----------------------------------------------------------------------------
# - process section                                                          -
# ----------------------------------------------------------------------------

# connect the socket with a tcp socket

trans afnix:std:tls:inets:accept-tcp-socket nil {
  # create a tcp server
  const this:ssrv (afnix:net:TcpServer (this:prms:to-server-sock-params))
  # accept with a tcp socket
  const s (this:ssrv:accept)
  # create a tls connect object
  const co (afnix:tls:TlsConnect true this:prms)
  # get the connected socket state
  trans ssta (co:connect s s)
  # create a tls socket
  afnix:tls:TlsSocket s ssta
}

# connect the socket with a tls socket
trans afnix:std:tls:inets:accept-tls-socket nil {
  # create a tls server
  const this:ssrv (afnix:tls:TlsServer this:prms)
  # accept a connection
  this:ssrv:accept
}

# run the tls read loop
# @param is the input stream
trans afnix:std:tls:inets:read-loop (is) {
  # loop in the input stream
  try {
    # loop until eof
    while (is:valid-p) {
      trans line (is:readln)
      println line
    }
  } {
    errorln "[inets] " what:about
    afnix:sys:exit 1
  }
}

# run the tls write loop
# @param os the output stream
trans afnix:std:tls:inets:write-loop (os) {
  # get the interpreter terminal
  const ts (afnix:sio:Terminal)
  ts:set-no-prompt true
  # loop until eof
  try {
    trans eos false
    while (not eos) {
      trans line (ts:read-line)
      trans eos  (line:eos-p)
      if (not eos) (os:write line)
    }
  } {
    errorln "[inets] " what:about
    afnix:sys:exit 1
  }
}
