# ----------------------------------------------------------------------------
# - std-acl-preld                                                            -
# - afnix:std:acl preload unit                                               -
# ----------------------------------------------------------------------------
# - This program is  free software;  you can  redistribute it and/or  modify -
# - it provided that this copyright notice is kept intact.                   -
# -                                                                          -
# - This  program  is  distributed in the hope  that it  will be useful, but -
# - without  any   warranty;  without  even   the   implied    warranty   of -
# - merchantability  or fitness for a particular purpose. In not event shall -
# - the copyright holder be  liable for  any direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.      -
# ----------------------------------------------------------------------------
# - copyright (c) 1999-2023 amaury darsch                                    -
# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# - library section                                                          -
# ----------------------------------------------------------------------------

interp:library "afnix-sio"

# ----------------------------------------------------------------------------
# - nameset section                                                          -
# ----------------------------------------------------------------------------

try (const afnix         (nameset))
try (const afnix:std     (nameset afnix))
try (const afnix:std:acl (nameset afnix:std))

try (const AFNIX         (nameset))
try (const AFNIX:STD     (nameset AFNIX))
try (const AFNIX:STD:ACL (nameset AFNIX:STD))
