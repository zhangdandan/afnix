# ---------------------------------------------------------------------------
# - AXI0081.als                                                             -
# - afnix engine test module                                                -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2023 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   apply special form test
# @author amaury darsch

# create a closure
const g nil (eval 1)
assert 1 (g)
assert 1 (apply g)

# create a closure
trans l (x) (eval x)
assert 1 (l 1)
assert 1 (apply l (1))

# create a class
const c (class)
trans c:g nil (eval 2)
trans c:l (x) (eval x)
const c:h nil (eval 4)
const c:i (x) (eval x)

const o (c)
assert 2 (o:g)
assert 2 (apply o g nil)
assert 2 (apply (c) g nil)
assert 5 (o:l 5)
assert 5 (apply o l (5))
