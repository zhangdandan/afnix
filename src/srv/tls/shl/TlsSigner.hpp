// ---------------------------------------------------------------------------
// - TlsSigner.hpp                                                           -
// - afnix:tls service - tls protocol signer class definition                -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_TLSSIGNER_HPP
#define  AFNIX_TLSSIGNER_HPP

#ifndef  AFNIX_TLSSTATE_HPP
#include "TlsState.hpp"
#endif

namespace afnix {

  /// The TlsSigner class is a tls protocol signer which generates a chunk
  /// ready for integration in a protocol message.
  /// @author amaury darsch

  class TlsSigner : public Object {
  protected:
    /// the tls state
    TlsState* p_ssta;

  public:
    /// create a tls signer by state
    /// @param ssta the tls state
    TlsSigner (TlsState* ssta);

    /// destroy this tls signer
    ~TlsSigner (void);

    /// @return the class name
    String repr (void) const;

    /// reset this object
    void reset (void);

    /// @return the info as a plist
    Plist getinfo (void) const;

    /// @return a digitally signed chunk
    TlsChunk tochunk (void) const;
    
  private:
    // make the copy constructor private
    TlsSigner (const TlsSigner&) =delete;
    // make the assignment operator private
    TlsSigner& operator = (const TlsSigner&) =delete;
  };
}

#endif
