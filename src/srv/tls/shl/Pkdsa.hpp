// ---------------------------------------------------------------------------
// - Pkdsa.hpp                                                               -
// - afnix:tls service - public key cryptographic standard class definition  -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_PKDSA_HPP
#define  AFNIX_PKDSA_HPP

#ifndef  AFNIX_PKI_HPP
#include "Pki.hpp"
#endif

#ifndef  AFNIX_KEY_HPP
#include "Key.hpp"
#endif

namespace afnix {

  /// The Pkdsa is the integration of the dsa standard into a public key
  /// infrastruture. In the case of dsa, both parameters and key can be
  /// generated.
  /// @author amaury darsch

  class Pkdsa : public Pki {
  private:
    /// the pki key
    Key* p_pkey;

  public:
    /// create a default key
    Pkdsa (void);

    /// create a pki key by key
    /// @param pkey the key to set
    Pkdsa (Key* pkey);
    
    /// create a pki key by path
    /// @param path the input file path
    Pkdsa (const String& path);
    
    /// copy construct a pki key object
    /// @param that the object to copy
    Pkdsa (const Pkdsa& that);

    /// destroy this pki key
    ~Pkdsa (void);
    
    /// @return the class name
    String repr (void) const;

    /// @return a clone of this object
    Object* clone (void) const;

    /// assign a pki key to this one
    /// @param that the object to copy
    Pkdsa& operator = (const Pkdsa& that);

    /// reset this pki object
    void reset (void);

    /// @return the pki key
    virtual Key* getkey (void) const;

  protected:
    /// encode a pki buffer
    bool encode (void);

    /// decode a pki buffer
    bool decode (void);
    
  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);
    
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const;
    
    /// apply this object with a set of arguments and a quark
    /// @param zobj  the current evaluable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Evaluable* zobj, Nameset* nset, const long quark,
		   Vector* argv);
  };
}

#endif
