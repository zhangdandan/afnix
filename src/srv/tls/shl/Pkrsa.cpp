// ---------------------------------------------------------------------------
// - Pkrsa.cpp                                                               -
// - afnix:tls service - public key cryptographic standard implementation    -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Pkrsa.hpp"
#include "Vector.hpp"
#include "Evaluable.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"
#include "AsnBuffer.hpp"
#include "AsnInteger.hpp"
#include "AsnSequence.hpp"
#include "transient.tcc"

namespace afnix {

  // -------------------------------------------------------------------------
  // - private section                                                       -
  // -------------------------------------------------------------------------

  // get the pkcs1 node from the asn buffer
  static AsnSequence* pkrsa_topseq (const AsnBuffer& abuf) {
    // map the buffer to a node
    AsnNode* node = abuf.mapnode ();
    // check for a sequence
    AsnSequence* pseq = dynamic_cast <AsnSequence*> (node);
    if (pseq == nullptr) {
      delete node;
      throw Exception ("pkrsa-error", "cannot map pkcs sequence node");
    }
    // check the sequence length
    if (pseq->getnlen () != 9) {
      delete node;
      throw Exception ("pkrsa-error", "invalid pkcs sequence length");
    }
    return pseq;
  }

  // get the pkcs1 relatif number by index
  static Relatif pkrsa_torval (const AsnSequence* pseq, const long index) {
    // check for nil
    if (pseq == nullptr) {
      throw Exception ("pkrsa-error", "invalid nil pkcs sequence node");
    }
    // get the node by index
    auto inod = dynamic_cast <AsnInteger*> (pseq->getnode (index));
    if (inod == nullptr) {
      throw Exception ("pkrsa-error", "cannot map integer node");
    }
    return inod->torelatif ();
  }

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a default pkcs key
  
  Pkrsa::Pkrsa (void) {
    p_pkey = nullptr;
    reset ();
  }

  // create a pkcs key by key

  Pkrsa::Pkrsa (Key* pkey) {
    // check for valid key
    if ((pkey != nullptr) && (pkey->gettype () != Key::CKEY_KRSA)) {
      throw Exception ("pkrsa-error" "invalid key type for pkcs");
    }
    if (pkey != nullptr) {
      d_pemc = pkey->ispublic () ? Pem::PEMC_RSAK : Pem::PEMC_RSAP;
    }
    Object::iref (p_pkey = pkey);
  }

  // create a pkcs key by path

  Pkrsa::Pkrsa (const String& path) {
    // preset objects
    p_pkey = nullptr;
    // read by path
    Pki::read (path);
    if (p_pkey != nullptr) {
      d_pemc = p_pkey->ispublic () ? Pem::PEMC_RSAK : Pem::PEMC_RSAP;
    }
  }
  
  // copy construct this pkcs key

  Pkrsa::Pkrsa (const Pkrsa& that) {
    that.rdlock ();
    try {
      // assign base object
      Pki::operator = (that);
      // copy locally
      p_pkey = (that.p_pkey == nullptr) ? nullptr : new Key (*that.p_pkey);
      Object::iref (p_pkey);
      // here it is
      that.unlock ();
    } catch (...) {
      that.unlock ();
      throw;
    }
  }
      
  // destroy this pkcs key

  Pkrsa::~Pkrsa (void) {
    Object::dref (p_pkey);
  }

  // assign a pkcs key to this one

  Pkrsa& Pkrsa::operator = (const Pkrsa& that) {
    // check for self-assignation
    if (this == &that) return *this;
    // lock and assign
    wrlock ();
    that.rdlock ();
    try {
      // assign base object
      Pki::operator = (that);
      // copy locally
      Object::dref (p_pkey);
      p_pkey = (that.p_pkey == nullptr) ? nullptr : new Key (*that.p_pkey);
      Object::iref (p_pkey);
      // here it is
      unlock ();
      that.unlock ();
      return *this;
    } catch (...) {
      unlock ();
      that.unlock ();
      throw;
    }
  }
  
  // return the object class name

  String Pkrsa::repr (void) const {
    return "Pkrsa";
  }

  // return a clone of this object

  Object* Pkrsa::clone (void) const {
    return new Pkrsa (*this);
  }
  
  // reset the pkcs key

  void Pkrsa::reset (void) {
    wrlock ();
    try {
      // reset base object
      Pki::reset ();
      // reset locally
      Object::dref (p_pkey); p_pkey = nullptr;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the pkcs key

  Key* Pkrsa::getkey (void) const {
    rdlock ();
    try {
      Key* result = p_pkey;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // -------------------------------------------------------------------------
  // - protected section                                                     -
  // -------------------------------------------------------------------------

  // encode the pkcs buffer

  bool Pkrsa::encode (void) {
    wrlock ();
    try {
      //extract the key data
      Relatif pmod = p_pkey->getrkey (Key::KRSA_PMOD);
      Relatif pexp = p_pkey->getrkey (Key::KRSA_PEXP);
      Relatif sexp = p_pkey->getrkey (Key::KRSA_SEXP);
      Relatif pprm = p_pkey->getrkey (Key::KRSA_PPRM);
      Relatif qprm = p_pkey->getrkey (Key::KRSA_QPRM);
      Relatif crtp = p_pkey->getrkey (Key::KRSA_CRTP);
      Relatif crtq = p_pkey->getrkey (Key::KRSA_CRTQ);
      Relatif crti = p_pkey->getrkey (Key::KRSA_CRTI);
      // create a sequence
      t_transient<AsnSequence> pseq = new AsnSequence;
      // add the version
      pseq->add (new AsnInteger(0));
      // add the key data
      pseq->add (new AsnInteger(pmod));
      pseq->add (new AsnInteger(pexp));
      pseq->add (new AsnInteger(sexp));
      pseq->add (new AsnInteger(pprm));
      pseq->add (new AsnInteger(qprm));
      pseq->add (new AsnInteger(crtp));
      pseq->add (new AsnInteger(crtq));
      pseq->add (new AsnInteger(crti));
      // write the sequence
      pseq->write (d_xbuf);
      // clean and return
      unlock ();
      return true;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // decode the pkcs buffer
  
  bool Pkrsa::decode (void) {
    wrlock ();
    try {
      // create an asn buffer
      AsnBuffer abuf (d_xbuf);
      // map the buffer to a sequence
      t_transient<AsnSequence> pseq = pkrsa_topseq (abuf);
      // get the object version
      long vers = pkrsa_torval(*pseq, 0).tolong ();
      if (vers != 0L) {
	throw Exception ("pkrsa-error", "invalid pkcs key version");
      }
      // get the key element
      Relatif pmod = pkrsa_torval (*pseq, 1);
      Relatif pexp = pkrsa_torval (*pseq, 2);
      Relatif sexp = pkrsa_torval (*pseq, 3);
      Relatif pprm = pkrsa_torval (*pseq, 4);
      Relatif qprm = pkrsa_torval (*pseq, 5);
      Relatif crtp = pkrsa_torval (*pseq, 6);
      Relatif crtq = pkrsa_torval (*pseq, 7);
      Relatif crti = pkrsa_torval (*pseq, 8);
      // create a key vector argument
      Vector kvec;
      kvec.add (new Relatif (pmod));
      kvec.add (new Relatif (pexp));
      kvec.add (new Relatif (sexp));
      kvec.add (new Relatif (pprm));
      kvec.add (new Relatif (qprm));
      kvec.add (new Relatif (crtp));
      kvec.add (new Relatif (crtq));
      kvec.add (new Relatif (crti));
      // create the key
      Object::iref (p_pkey = new Key (Key::CKEY_KRSA, kvec));
      // unlock and return
      unlock ();
      return true;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 1;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_GETKEY = zone.intern ("get-key");

  // create a new object in a generic way
 
  Object* Pkrsa::mknew (Vector* argv) {
    long argc = (argv == nullptr) ? 0 : argv->length ();

    // create a default object
    if (argc == 0) return new Pkrsa;
    // check for 1 argument
    if (argc == 1) {
      Object* obj = argv->get (0);
      // check for a string
      String* path = dynamic_cast <String*> (obj);
      if (path != nullptr) return new Pkrsa (*path);
      // check for a key
      Key* key = dynamic_cast <Key*> (obj);
      if (key != nullptr) return new Pkrsa (key);
      // invalid object
      throw Exception ("type-error", "invalif object with pkcs",
		       Object::repr (obj));
    }
    // too many arguments
    throw Exception ("argument-error",
                     "too many argument with pkcs constructor");
  }

  // return true if the given quark is defined

  bool Pkrsa::isquark (const long quark, const bool hflg) const {
    rdlock ();
    try {
      if (zone.exists (quark) == true) {
	unlock ();
	return true;
      }
      bool result = hflg ? Pki::isquark (quark, hflg) : false;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // apply this object with a set of arguments and a quark

  Object* Pkrsa::apply (Evaluable* zobj, Nameset* nset, const long quark,
			Vector* argv) {
    // get the number of arguments
    long argc = (argv == nullptr) ? 0 : argv->length ();
    
    // check for 0 argument
    if (argc == 0) {
      if (quark == QUARK_GETKEY) {
	rdlock ();
	try {
	  Object* result = getkey ();
	  zobj->post (result);
	  unlock ();
	  return result;
	} catch (...) {
	  unlock ();
	  throw;
	}
      }
    }
    // call the pki method
    return Pki::apply (zobj, nset, quark, argv);
  }
}
