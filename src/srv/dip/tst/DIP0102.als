# ---------------------------------------------------------------------------
# - DIP0102.als                                                             -
# - afnix:dip service test unit                                             -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2023 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   netpbm test unit
# @author amaury darsch

# get the service
interp:library "afnix-dip"

# create an image by file
const pixm (afnix:dip:netpbm-read "DIP0102.ppm")
assert true (afnix:dip:pixmap-p pixm)
# check format geometry
assert afnix:dip:Pixel:PFMT-RGBA (pixm:get-format)
assert 48 (pixm:get-width)
assert 32 (pixm:get-height)

# create a cube map
const cubm (afnix:dip:Cubmap pixm)
assert true (afnix:dip:cubmap-p cubm)
assert 48 (cubm:get-width)
assert 32 (cubm:get-height)
