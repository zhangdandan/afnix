# ---------------------------------------------------------------------------
# - TCZ0030.als                                                             -
# - afnix:tcz service test unit                                             -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2023 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   part test unit
# @author amaury darsch

# get the service
interp:library "afnix-tcz"

# create a nil part
trans part (afnix:tcz:Part)

# check predicate and representation
assert true   (afnix:tcz:part-p part)
assert "Part" (part:repr)

# check kid
const kid (part:get-kid)
assert true (part:kid-p (kid:to-string))
