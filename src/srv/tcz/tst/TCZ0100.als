# ---------------------------------------------------------------------------
# - TCZ0100.als                                                             -
# - afnix:tcz service test unit                                             -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2023 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   transmuter test unit
# @author amaury darsch

# get the module
interp:library "afnix-tcz"

# create a transmuter
const trmr (afnix:tcz:Transmuter)
assert true (afnix:tcz:transmuter-p trmr)
assert true (afnix:tcz:transmutable-p trmr)
assert "Transmuter" (trmr:repr)

# create a test kernel
const text "assert 1 (interp:argv:length)"
const kern (Module (Buffer text))
# bind the kernel
assert true (trmr:bind kern)
# bind an empty mixture
assert true (trmr:bind (afnix:tcz:Mixture))
# process an empty mixture
assert true (trmr:process)
