// ---------------------------------------------------------------------------
// - Libtcz.cpp                                                              -
// - afnix:tcz service - declaration & implementation                        -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Meta.hpp"
#include "Visa.hpp"
#include "Tczsrl.hxx"
#include "Libtcz.hpp"
#include "Domain.hpp"
#include "Notary.hpp"
#include "Tracker.hpp"
#include "Predtcz.hpp"
#include "TczCalls.hpp"
#include "Function.hpp"
#include "Delegate.hpp"
#include "LocalZone.hpp"
#include "RealmZone.hpp"
#include "UserSpace.hpp"
#include "Transmuter.hpp"

namespace afnix {

  // initialize the afnix:tcz service

  Object* init_afnix_tcz (Interp* interp, Vector* argv) {
    // make sure we are not called from something crazy
    if (interp == nullptr) return nullptr;
    // bind the serial dispatcher
    if (Serial::isvdid (SRL_DEOD_DID) == false) {
      throw Exception ("interp-error", "invalid tcz serial dispatcher");
    }
    // create the afnix:tcz nameset
    Nameset* aset = interp->mknset ("afnix");
    Nameset* gset = aset->mknset   ("tcz");

    // bind all symbols in the afnix:tcz nameset
    gset->symcst ("Tcz",              new Meta (Tcz::meval));
    gset->symcst ("Act",              new Meta (Act::mknew));
    gset->symcst ("Blob",             new Meta (Blob::mknew));
    gset->symcst ("Bloc",             new Meta (Bloc::mknew));
    gset->symcst ("Visa",             new Meta (Visa::mknew));
    gset->symcst ("Part",             new Meta (Part::mknew));
    gset->symcst ("Datum",            new Meta (Datum::mknew));
    gset->symcst ("Whois",            new Meta (Whois::mknew));
    gset->symcst ("Whatis",           new Meta (Whatis::mknew));
    gset->symcst ("Domain",           new Meta (Domain::mknew));
    gset->symcst ("Notary",           new Meta (Notary::mknew));
    gset->symcst ("Carrier",          new Meta (Carrier::mknew));
    gset->symcst ("Tracker",          new Meta (Tracker::mknew));
    gset->symcst ("Mixture",          new Meta (Mixture::mknew));
    gset->symcst ("Identity",         new Meta (Identity::mknew));
    gset->symcst ("Delegate",         new Meta (Delegate::mknew));
    gset->symcst ("LocalZone",        new Meta (LocalZone::mknew));
    gset->symcst ("RealmZone",        new Meta (RealmZone::mknew));
    gset->symcst ("UserSpace",        new Meta (UserSpace::mknew));
    gset->symcst ("Authority",        new Meta (Authority::mknew));
    gset->symcst ("Principal",        new Meta (Principal::mknew));
    gset->symcst ("Collection",       new Meta (Collection::mknew));
    gset->symcst ("LocalSpace",       new Meta (LocalSpace::mknew));
    gset->symcst ("Transmuter",       new Meta (Transmuter::mknew));
    gset->symcst ("Credential",       new Meta (Credential::meval,
						Credential::mknew));

    // bind the predicates
    gset->symcst ("act-p",            new Function (tcz_xactp));
    gset->symcst ("part-p",           new Function (tcz_partp));
    gset->symcst ("blob-p",           new Function (tcz_blobp));
    gset->symcst ("bloc-p",           new Function (tcz_blocp));
    gset->symcst ("visa-p",           new Function (tcz_visap));
    gset->symcst ("realm-p",          new Function (tcz_relmp));
    gset->symcst ("datum-p",          new Function (tcz_dtump));
    gset->symcst ("whois-p",          new Function (tcz_woisp));
    gset->symcst ("whatis-p",         new Function (tcz_waisp));
    gset->symcst ("domain-p",         new Function (tcz_domnp));
    gset->symcst ("notary-p",         new Function (tcz_ntryp));
    gset->symcst ("carrier-p",        new Function (tcz_cblbp));
    gset->symcst ("tracker-p",        new Function (tcz_tckrp));
    gset->symcst ("mixture-p",        new Function (tcz_mixtp));
    gset->symcst ("identity-p",       new Function (tcz_idtyp));
    gset->symcst ("delegate-p",       new Function (tcz_dblbp));
    gset->symcst ("workzone-p",       new Function (tcz_wzonp));
    gset->symcst ("workspace-p",      new Function (tcz_wspcp));
    gset->symcst ("localzone-p",      new Function (tcz_lzonp));
    gset->symcst ("realmzone-p",      new Function (tcz_rzonp));
    gset->symcst ("userspace-p",      new Function (tcz_uspcp));
    gset->symcst ("authority-p",      new Function (tcz_authp));
    gset->symcst ("principal-p",      new Function (tcz_pcplp));
    gset->symcst ("collection-p",     new Function (tcz_collp));
    gset->symcst ("localspace-p",     new Function (tcz_lspcp));
    gset->symcst ("credential-p",     new Function (tcz_credp));
    gset->symcst ("transmuter-p",     new Function (tcz_trmrp));
    gset->symcst ("transmutable-p",   new Function (tcz_trmtp));

    // bind other functions
    gset->symcst ("to-workspace-uri", new Function (tcz_tozuri));

    // not used but needed
    return nullptr;
  }
}

extern "C" {
  afnix::Object* dli_afnix_tcz (afnix::Interp* interp, afnix::Vector* argv) {
    return init_afnix_tcz (interp, argv);
  }
}
