// ---------------------------------------------------------------------------
// - Transmutable.hpp                                                        -
// - afnix:tcz service - mixture transmutation abstract class definition     -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_TRANSMUTABLE_HPP
#define  AFNIX_TRANSMUTABLE_HPP

#ifndef  AFNIX_LOGGER_HPP
#include "Logger.hpp"
#endif

#ifndef  AFNIX_MIXTURE_HPP
#include "Mixture.hpp"
#endif

namespace afnix {

  /// The Transmutable class is a mixture transformation interface. Given a
  /// set of bounded mixture, the 'process' method updates the output mixture,
  /// generally with the help of a kernel.
  /// @author amaury darsch

  class Transmutable : public Object {
  public:
    /// create a default transmutable
    Transmutable (void) =default;

    /// reset this transmutable
    virtual void reset (void) =0;

    /// stage this transmutable
    virtual bool stage (void) =0;

    /// process with a logger
    /// @param logr the logger object
    virtual bool process (Logger* logr) =0;

    /// bind a mixture
    /// @param mixt the mixture to bind
    virtual bool bind (Mixture* mixt) =0;
    
  public:
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const override;
    
    /// apply this object with a set of arguments and a quark
    /// @param zobj  the current evaluable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments  to apply
    Object* apply (Evaluable* zobj, Nameset* nset, const long quark,
                   Vector* argv) override;
  };
}

#endif
