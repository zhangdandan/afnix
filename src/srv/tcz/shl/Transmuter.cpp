// ---------------------------------------------------------------------------
// - Transmuter.cpp                                                          -
// - afnix:tcz service - mixture transmutation class implementation            -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Vector.hpp"
#include "Boolean.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"
#include "Transmuter.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a default transmuter

  Transmuter::Transmuter (void) {
    p_kern = nullptr;
    p_hpri = nullptr;
  }

  // create a transmuter by module

  Transmuter::Transmuter (Module* kern) {
    p_kern = nullptr;
    p_hpri = nullptr;
    if (bind (kern) == false) {
      throw Exception ("transmuter-error", "cannot bind kernel");
    }
  }
  
  // destroy this transmuter

  Transmuter::~Transmuter (void) {
    Object::dref (p_kern);
    Object::dref (p_hpri);
  }

  // return the mixture class name
  
  String Transmuter::repr (void) const {
    return "Transmuter";
  }
  
  // reset this transmuter

  void Transmuter::reset (void) {
    wrlock ();
    try {
      Object::dref (p_hpri); p_hpri = nullptr;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // stage the transmuter

  bool Transmuter::stage (void) {
    wrlock ();
    try {
      // check for staging
      if (p_hpri != nullptr) {
	unlock ();
	return true;
      }
      // create a hyper interpreter
      Object::iref (p_hpri = new Hyperp);
      return true;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // process with a kernel

  bool Transmuter::process (Logger* logr) {
    wrlock ();
    try {
      // stage the transmuter
      if (stage () == false) {
	throw Exception ("transmuter-error", "cannot stage for process");
      }
      // load the module
      bool status = (p_kern != nullptr) ? p_hpri->load (*p_kern) : false;
      unlock ();
      return status;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // bind a mixture

  bool Transmuter::bind (Mixture* mixt) {
    wrlock ();
    try {
      // stage the transmuter
      if (stage () == false) {
	throw Exception ("transmuter-error", "cannot stage for binding");
      }
      // add the mixture to the hyper arguments
      bool status = false;
      if ((mixt != nullptr) && (p_hpri != nullptr)) {
	p_hpri->getargv()->add (mixt);
	status = true;
      }
      unlock ();
      return status;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // bind a kernel to this transmuter

  bool Transmuter::bind (Module* kern) {
    wrlock ();
    try {
      Object::iref (kern); Object::dref (p_kern); p_kern = kern;
      unlock ();
      return true;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 1;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_BIND = zone.intern ("bind");

  // create a new object in a generic way

  Object* Transmuter::mknew (Vector* argv) {
    long argc = (argv == nullptr) ? 0 : argv->length ();
    // check for 0 argument
    if (argc == 0) return new Transmuter;
    // check for 1 argument
    if (argc == 1) {
      Object* obj = argv->get (0);
      // chck for a module
      auto kern = dynamic_cast<Module*>(obj);
      if (kern != nullptr) return new Transmuter (kern);
      // invalid object
      throw Exception ("type-error", "invalid object for transmuter",
		       Object::repr (obj));
    }
    // invalid argument
    throw Exception ("argument-error",
                     "too many argument with transmuter constructor");
  }
  
  // return true if the given quark is defined

  bool Transmuter::isquark (const long quark, const bool hflg) const {
    rdlock ();
    try {
      if (zone.exists (quark) == true) {
	unlock ();
	return true;
      }
      bool result = hflg ? Transmutable::isquark (quark, hflg) : false;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // apply this object with a set of arguments and a quark
  
  Object* Transmuter::apply (Evaluable* zobj, Nameset* nset, const long quark,
			     Vector* argv) {
    // get the number of arguments
    long argc = (argv == nullptr) ? 0 : argv->length ();

    // check for 1 argument
    if (argc == 1) {
      if (quark == QUARK_BIND) {
	Object* obj = argv->get (0);
	// check for a module
	auto kern = dynamic_cast <Module*> (obj);
	if (kern != nullptr) return new Boolean (bind (kern));
      }
    }
    // call the transmutable method
    return Transmutable::apply (zobj, nset, quark, argv);
  }
}
