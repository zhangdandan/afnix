// ----------------------------------------------------------------------------
// - Datum.hpp                                                                -
// - afnix:tcz service - datum part class definition                          -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_DATUM_HPP
#define  AFNIX_DATUM_HPP

#ifndef  AFNIX_TCZ_HPP
#include "Tcz.hpp"
#endif

#ifndef  AFNIX_PART_HPP
#include "Part.hpp"
#endif

namespace afnix {

  /// The Datum class is a mixture part which contains the data used by a
  /// a generic interface. The datum is mostly identified by the object
  /// type and mode.
  /// @author amaury darsch

  class Datum : public Part {
  protected:
    /// the content mode
    Tcz::t_cmod d_cmod;
    /// the datum value
    Object* p_rval;
    
  public:
    /// create a default datum
    Datum (void);

    /// create a datum by name
    /// @param name the datum name
    Datum (const String& name);

    /// create a datum by name and info
    /// @param name the datum name
    /// @param info the datum info
    Datum (const String& name, const String& info);

    /// copy construct this datum
    /// @param that the datum to copy
    Datum (const Datum& that);

    /// copy move this datum
    /// @param that the datum to move
    Datum (Datum&& that) noexcept;
    
    /// destroy this datum
    ~Datum (void);

    /// assign a compute datum to this one
    /// @param that the datum to assign
    Datum& operator = (const Datum& that);

    /// move a compute datum to this one
    /// @param that the datum to move
    Datum& operator = (Datum&& that) noexcept;
    
    /// @return the class name
    String repr (void) const override;

    /// @return a clone of this object
    Object* clone (void) const override;

    /// @return the serial did
    t_word getdid (void) const override;

    /// @return the serial sid
    t_word getsid (void) const override;
    
    /// serialize this blob
    /// @param os the output stream
    void wrstream (OutputStream& os) const override;

    /// deserialize this blob
    /// @param is the input stream
    void rdstream (InputStream& os) override;

    /// @return the object plist
    Plist getplst (void) const override;

    /// @return a datum object view
    HashTable toview (void) const override;
    
    /// bind the datum
    /// @param cmod the content mode
    /// @param rval the datum object
    virtual bool bind (const Tcz::t_cmod cmod, Object* rval);

    /// @return the content mode
    virtual Tcz::t_cmod getcmod (void) const;

    /// @return the datum value
    virtual Object* getdval (void) const;

  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);
    
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const override;
    
    /// apply this object with a set of arguments and a quark
    /// @param zobj  the current evaluable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments  to apply
    Object* apply (Evaluable* zobj, Nameset* nset, const long quark,
                   Vector* argv) override;
  };
}

#endif
