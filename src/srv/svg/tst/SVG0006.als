# ---------------------------------------------------------------------------
# - SVG0006.als                                                             -
# - afnix:svg service test unit                                             -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2023 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   svg line test unit
# @author amaury darsch

# get the module
interp:library "afnix-xml"
interp:library "afnix-svg"

# create a svg line
const  line (afnix:svg:SvgLine 100 300 300 100)
assert true (afnix:xml:node-p line)
assert true (afnix:svg:svg-styling-p line)
assert true (afnix:svg:svg-line-p line)

# set some styling
line:set-stroke "sienna"
line:set-stroke-width 10
