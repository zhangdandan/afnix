# ---------------------------------------------------------------------------
# - NWG0013.als                                                             -
# - afnix:nwg module test unit                                              -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2023 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   uuid test unit
# @author amaury darsch

# get the module
interp:library "afnix-nwg"

# nil json
trans json (afnix:nwg:Json)
assert true (afnix:nwg:json-p json)
assert "Json" (json:repr)

# check for nil object
assert nil (json:parse)

# check a boolean literal
json:reset
assert true (json:stringify true)
assert "true" (json:to-string)
assert true   (json:parse)

# check an integer literal
json:reset
assert true (json:stringify 2000)
assert "2000" (json:to-string)
assert 2000   (json:parse)

# check a real literal
json:reset
assert true (json:stringify 2000.0)
assert "2000.000000" (json:to-string)
assert 2000.0   (json:parse)

# check a string literal
json:reset
assert true (json:stringify "json")
assert "\"json\"" (json:to-string)
assert "json" (json:parse)

# check a vector
const jvec (Vector 2000 "json" true)
json:reset
assert true (json:stringify jvec)
assert "[2000,\"json\",true]" (json:to-string)
const vobj (json:parse)
assert (jvec:length) (vobj:length)
for (u v) (jvec vobj) (assert u v)

# check a set
const jset (Set 2000 "json" true)
json:reset
assert true (json:stringify jset)
const sobj (json:parse)
assert (jset:length) (sobj:length)

# check a hashtable - keys makes the order
const jhsh (HashTable)
jhsh:add "0" 2000
jhsh:add "1" "json"
jhsh:add "2" true
json:reset
assert true (json:stringify jhsh)
assert "{\"0\":2000,\"1\":\"json\",\"2\":true}" (json:to-string)
const hobj (json:parse)
assert (jhsh:length) (hobj:length)
assert (jhsh:lookup "0") (hobj:lookup "0")
assert (jhsh:lookup "1") (hobj:lookup "1")
assert (jhsh:lookup "2") (hobj:lookup "2")

# check a plist 
const jlst (Plist)
jlst:add "0" 2000
jlst:add "1" "json"
jlst:add "2" true
json:reset
assert true (json:stringify jlst)
assert "{\"0\":2000,\"1\":\"json\",\"2\":true}" (json:to-string)
const pobj (json:parse)
assert (jlst:length) (pobj:length)

# check a printtable 
const jtbl (PrintTable 2)
trans row (jtbl:add)
jtbl:set row 0 "00"
jtbl:set row 1 "01"
trans row (jtbl:add)
jtbl:set row 0 "10"
jtbl:set row 1 "11"
json:reset
assert true (json:stringify jtbl)
assert "[[\"00\",\"01\"],[\"10\",\"11\"]]"(json:to-string)

# extra json parse
trans sbuf "[0, [1, 2], 3]"
trans jobj (json:parse sbuf)
trans vsub (jobj:get 1)
assert 1 (vsub:get 0)
assert 2 (vsub:get 1)
assert 0 (jobj:get 0)
assert 3 (jobj:get 2)

# complex parse
trans sbuf "{ \"0\" : 0, \"1\" : [1, 2], \"2\" : {\"3\" : 3, \"4\" : 4} }"
trans jobj (json:parse sbuf)
assert 0   (jobj:lookup "0")
trans vsub (jobj:lookup "1")
assert 1   (vsub:get 0)
assert 2   (vsub:get 1)
trans hsub (jobj:lookup "2")
assert 3   (hsub:lookup "3")
assert 4   (hsub:lookup "4")
