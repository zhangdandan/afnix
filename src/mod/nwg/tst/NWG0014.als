# ---------------------------------------------------------------------------
# - NWG0014.als                                                             -
# - afnix:nwg module test unit                                              -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2023 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info  percent encoding test
# @author amaury darsch

# get the module
interp:library "afnix-nwg"

# space test
trans  sval "hello world"
trans  uval "hello%20world"
trans  pval "hello%20world"
trans  cval "hello%20world"
trans  wval "hello+world"
assert uval (afnix:nwg:uri-encode       sval)
assert pval (afnix:nwg:percent-encode   sval)
assert cval (afnix:nwg:component-encode sval)
assert wval (afnix:nwg:www-form-encode  sval)

# character test
trans  sval ";,/?:@&=+$-_.!~*'()#"
trans  cval "%3B%2C%2F%3F%3A%40%26%3D%2B%24-_.!~*'()%23"
trans  wval "%3B%2C%2F%3F%3A%40%26%3D%2B%24-_.%21%7E*%27%28%29%23"
assert sval (afnix:nwg:uri-encode sval)
assert sval (afnix:nwg:percent-encode sval)
assert cval (afnix:nwg:component-encode sval)
assert wval (afnix:nwg:www-form-encode sval)
