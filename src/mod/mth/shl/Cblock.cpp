// ---------------------------------------------------------------------------
// - Cblock.cpp                                                              -
// - afnix:mth module - complex block matrix implementation                  -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Cmd.hpp"
#include "Real.hpp"
#include "Math.hpp"
#include "Mthsid.hxx"
#include "Vector.hpp"
#include "Cblock.hpp"
#include "Algebra.hpp"
#include "Integer.hpp"
#include "Hilbert.hpp"
#include "Exception.hpp"
#include "transient.tcc"

namespace afnix {

  // -------------------------------------------------------------------------
  // - private section                                                       -
  // -------------------------------------------------------------------------

  // this procedure computes the minimum of two integers
  static inline t_long min (const t_long x, const t_long y) {
    return (x < y) ? x : y;
  }

  // -------------------------------------------------------------------------
  // - public section                                                        -
  // -------------------------------------------------------------------------

  // add a matrix with another one
  
  Cblock operator + (const Cblock& mx, const Cblock& my) {
    mx.rdlock ();
    my.rdlock ();
    try {
      // create a result matrix
      Cblock mr (mx.getrsiz (), my.getcsiz ());
      // add the matrices
      Algebra::add (mr, mx, my);
      // unlock and return
      mx.unlock ();
      my.unlock ();
      return mr;
    } catch (...) {
      mx.unlock ();
      my.unlock ();
      throw;
    }
  }

  // substract a matrix with another one

  Cblock operator - (const Cblock& mx, const Cblock& my) {
    mx.rdlock ();
    my.rdlock ();
    try {
      // create a result matrix
      Cblock mr (mx.getrsiz (), my.getcsiz ());
      // substract the matrices
      Algebra::sub (mr, mx, my);
      // unlock and return
      mx.unlock ();
      my.unlock ();
      return mr;
    } catch (...) {
      mx.unlock ();
      my.unlock ();
      throw;
    }
  }

  // multiply a matrix with a vector

  Cvector operator * (const Cblock& m, const Cvector& x) {
    m.rdlock ();
    x.rdlock ();
    try {
      // extract operating size
      t_long rows = m.getrsiz ();
      // create result vector
      Cvector r (rows);
      // perform the operation
      m.mul (r, x, 1.0);
      // unlock and return
      m.unlock ();
      x.unlock ();
      return r;
    } catch (...) {
      m.unlock ();
      x.unlock ();
      throw;
    }
  }

  // multiply two matrices

  Cblock operator * (const Cblock& mx, const Cblock& my) {
    mx.rdlock ();
    my.rdlock ();
    try {
      // create a result matrix
      Cblock mr (mx.getrsiz (), my.getcsiz ());
      // muliply the matrices
      Algebra::mul (mr, mx, my);
      // unlock and return
      mx.unlock ();
      my.unlock ();
      return mr;
    } catch (...) {
      mx.unlock ();
      my.unlock ();
      throw;
    }
  }

  // compute a matrix tensor product

  Cblock Cblock::product (const Cblock& mx, const Cblock& my) {
    mx.rdlock ();
    my.rdlock ();
    try {
      // create a result matrix
      Cblock mr (mx.getrsiz ()*my.getrsiz(), mx.getcsiz()*my.getcsiz ());
      // compute the matrix product
      Hilbert::product (mr, mx, my);
      // unlock and return
      mx.unlock ();
      my.unlock ();
      return mr;
    } catch (...) {
      mx.unlock ();
      my.unlock ();
      throw;
    }
  }

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a default matrix

  Cblock::Cblock (void) {
    p_blok = nullptr;
  }

  // create a square matrix by size

  Cblock::Cblock (const t_long size) : Cmi (size) {
    p_blok = nullptr;
    resize (size, size);
  }

  // create a matrix by size

  Cblock::Cblock (const t_long rsiz, const t_long csiz) : Cmi (rsiz, csiz) {
    p_blok = nullptr;
    resize (rsiz, csiz);    
  }

  // generate a matrix by vector product

  Cblock::Cblock (const Cvi& u, const Cvi& v) {
    // reset matrix
    p_blok = nullptr;
    // get the target size
    t_long rsiz = u.getsize ();
    t_long csiz = v.getsize ();
    // resize the matrix
    resize (rsiz, csiz);
    // update the matrix
    Hilbert::product (*this, u, v);
  }

  // create a matrix by trace
  
  Cblock::Cblock (const Cti& t) {
    // reset matrix
    p_blok = nullptr;
    // get the target size
    t_long tsiz = t.getsize ();
    // resize the matrix
    resize (tsiz, tsiz);
    // update the matrix
    Algebra::cpt (*this, t);
  }
  
  // generate a matrix by matrix product

  Cblock::Cblock (const Cmi& mx, const Cmi& my) {
    // reset matrix
    p_blok = nullptr;
    // get the target size
    t_long rsiz = mx.getrsiz () * my.getrsiz ();
    t_long csiz = mx.getcsiz () * my.getcsiz ();
    // resize the matrix
    resize (rsiz, csiz);
    // update the matrix
    Hilbert::product (*this, mx, my);
  }
  
  // copy construct this matrix

  Cblock::Cblock (const Cblock& that) {
    that.rdlock ();
    try {
      // allocate size
      p_blok = nullptr;
      resize (that.d_rsiz, that.d_csiz);
      // copy the original block
      t_long size = (2*d_rsiz) * (2*d_csiz);
      for (t_long k = 0LL; k < size; k++) p_blok[k] = that.p_blok[k];
      that.unlock ();
    } catch (...) {
      that.unlock ();
      throw;
    }
  }
	
  // destroy this matrix

  Cblock::~Cblock (void) {
    delete [] p_blok;
  }

  // assign a matrix to this one

  Cblock& Cblock::operator = (const Cblock& that) {
    // check for self-assignation
    if (this == &that) return *this;
    // lock and assign
    wrlock ();
    that.rdlock ();
    try {
      // delete the old block
      delete [] p_blok;
      // allocate size
      p_blok = nullptr;
      resize (that.d_rsiz, that.d_csiz);
      // copy the original block
      t_long size = (2*d_rsiz) * (2*d_csiz);
      for (t_long k = 0LL; k < size; k++) p_blok[k] = that.p_blok[k];
      // unlock and return
      unlock ();
      that.unlock ();
      return *this;
    } catch (...) {
      that.unlock ();
      throw;
    }
  }
  

  // return the class name

  String Cblock::repr (void) const {
    return "Cblock";
  }

  // return a clone of this object

  Object* Cblock::clone (void) const {
    return new Cblock (*this);
  }

  // return the serial did

  t_word Cblock::getdid (void) const {
    return SRL_DEOD_MTH;
  }

  // return the serial sid

  t_word Cblock::getsid (void) const {
    return SRL_CBLK_SID;
  }

  // return true if the matrix is null

  bool Cblock::isnil (void) const {
    wrlock ();
    try {
      bool result = true;
      t_long size = (2*d_rsiz) * (2*d_csiz);
      for (t_long k = 0LL; k < size; k++) {
	if (p_blok[k] != 0.0) {
	  result = false;
	  break;
	}
      }
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // reset this matrix

  void Cblock::reset (void) {
    wrlock ();
    try {
      // reset base
      Cmi::reset ();
      // reset locally
      delete [] p_blok; p_blok = nullptr;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // clear this matrix

  void Cblock::clear (void) {
    wrlock ();
    try {
      t_long size = (2*d_rsiz) * (2*d_csiz);
      for (t_long k = 0LL; k < size; k++) p_blok[k] = 0.0;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // resize this matrix

  void Cblock::resize (const t_long rsiz, const t_long csiz) {
    wrlock ();
    try {
      // check for valid size
      if ((rsiz < 0) || (csiz < 0)) {
	throw Exception ("cblock-error", "invalid resize parameters");
      }
      //  check for nil first
      if ((p_blok == nullptr) && (rsiz > 0LL) && (csiz > 0LL)) {
	// allocate the block
	t_long size = (2*rsiz) * (2*csiz);
	p_blok = new t_real[size];
	// local clear before size update
	for (long k = 0LL; k < size; k++) p_blok[k] = 0.0;
      } else {
	if ((d_rsiz != rsiz) || (d_csiz != csiz)) {
	  // allocate a new array
	  t_long  size = (2*rsiz) * (2*csiz);
	  t_real* blok = new t_real[size];
	  // compute mininum sizes
	  t_long rmin = 2*min (d_rsiz, rsiz);
	  t_long cmin = 2*min (d_csiz, csiz);
	  // copy the array
	  for (t_long i = 0LL; i < rmin; i++) {
	    for (t_long j = 0LL; j < cmin; j++) {
	      blok[i*(2*csiz)+j] = p_blok[i*(2*d_csiz)+j];
	    }
	  }
	  // set null values
	  for (t_long i = rmin; i < (2*rsiz); i++) {
	    for (t_long j = cmin; j < (2*csiz); j++) {
	      blok[i*(2*csiz)+j] = 0.0;
	    }
	  }
	  delete [] p_blok;
	  p_blok = blok;
	}
      }
      // update size and unlock
      d_rsiz = rsiz;
      d_csiz = csiz;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // create a zero identical matrix

  Cmi* Cblock::zeros (void) const {
    rdlock ();
    try {
      Cmi* result = new Cblock (d_rsiz, d_csiz);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // copy a matrix diagonal into a trace

  Cti* Cblock::cpt (void) const {
    rdlock ();
    try {
      // prepare result vector
      t_transient<Cti> result =
	(d_csiz == 0LL) ? nullptr : new Ctrace (d_csiz);
      // copy if not nil
      if (result.valid () == true) Algebra::cpt (**result, *this);
      unlock ();
      return result.detach ();
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // copy a matrix row into a vector

  Cvi* Cblock::cpr (const t_long row) const {
    rdlock ();
    Cvi* result = nullptr;
    try {
      // prepare result vector
      result = (d_csiz == 0LL) ? nullptr : new Cvector (d_csiz);
      // copy if not nil
      if (result != nullptr) Algebra::cpr (*result, *this, row);
      unlock ();
      return result;
    } catch (...) {
      delete result;
      unlock ();
      throw;
    }
  }

  // copy a matrix column into a vector

  Cvi* Cblock::cpc (const t_long col) const {
    rdlock ();
    Cvi* result = nullptr;
    try {
      // prepare result vector
      result = (d_rsiz == 0LL) ? nullptr : new Cvector (d_rsiz);
      // copy if not nil
      if (result != nullptr) Algebra::cpc (*result, *this, col);
      unlock ();
      return result;
    } catch (...) {
      delete result;
      unlock ();
      throw;
    }
  }

  // return a new matrix iterator

  Iterator* Cblock::makeit (void) {
    rdlock ();
    try {
      Iterator* result = new Cblockit (this);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // permutate this matrix

  Cmi* Cblock::permutate (const Cpi& p) const {
    rdlock ();
    Cmi* result = nullptr;
    try {
      // create a result matrix
      result = new Cblock (d_rsiz, d_csiz);
      // permutate this matrix
      Algebra::permutate (*result, *this, p);
      unlock ();
      return result;
    } catch (...) {
      delete result;
      unlock ();
      throw;
    }
  }

  // reverse permutate this matrix

  Cmi* Cblock::reverse (const Cpi& p) const {
    rdlock ();
    Cmi* result = nullptr;
    try {
      // create a result matrix
      result = new Cblock (d_rsiz, d_csiz);
      // permutate this vector
      Algebra::reverse (*result, *this, p);
      unlock ();
      return result;
    } catch (...) {
      delete result;
      unlock ();
      throw;
    }
  }

  // get the viewable size

  long Cblock::tosize (void) const {
    rdlock ();
    try {
      long result = (2*d_rsiz) * (2*d_csiz) * sizeof(t_real);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the viewable data

  t_byte* Cblock::tobyte (void) {
    wrlock ();
    try {
      auto result = reinterpret_cast<t_byte*>(p_blok);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // get the viewable data

  const t_byte* Cblock::tobyte (void) const {
    rdlock ();
    try {
      auto result = reinterpret_cast<const t_byte*>(p_blok);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // -------------------------------------------------------------------------
  // - no lock section                                                       -
  // -------------------------------------------------------------------------

  // no lock - set a matrix by position

  void Cblock::nlset (const t_long row, const t_long col, const Complex& val) {
    t_long idx = row * (2*d_csiz) + 2*col;
    auto  zval = reinterpret_cast<const t_real*>(val.tobyte());
    if (p_blok != nullptr) {
      p_blok[idx]   = zval[0];
      p_blok[idx+1] = zval[1];
    }
  }

  // set a matrix by position

  Complex Cblock::nlget (const t_long row, const t_long col) const {
    if (p_blok == nullptr) return 0.0;
    t_long idx = row * (2*d_csiz) + 2*col;
    Complex result (p_blok[idx], p_blok[idx+1]);
    return result;
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // create a new object in a generic way

  Object* Cblock::mknew (Vector* argv) {
    long argc = (argv == nullptr) ? 0 : argv->length ();
    
    // check for 0 argument
    if (argc == 0) return new Cblock;
    // check for 1 argument
    if (argc == 1) {
      // collect object
      Object* obj = argv->get (0);
      // check for integer
      auto iobj = dynamic_cast<Integer*>(obj);
      if (iobj != nullptr) return new Cblock (iobj->tolong ());
      // check for trace
      auto tobj = dynamic_cast<Cti*>(obj);
      if (tobj != nullptr) return new Cblock (*tobj);
      // invalid object
      throw Exception ("type-error", "invalid objects as cblock arguments");
    }
    // check for 2 arguments
    if (argc == 2) {
      Object* zobj = argv->get (0);
      Object* cobj = argv->get (1);
      // check for integers
      Integer* ri = dynamic_cast <Integer*> (zobj);
      Integer* ci = dynamic_cast <Integer*> (cobj);
      if ((ri != nullptr) && (ci != nullptr)) {
	t_long rsiz = ri->tolong ();
	t_long csiz = ci->tolong ();
	return new Cblock (rsiz, csiz);
      }
      // check for vectors
      Cvi* u = dynamic_cast <Cvi*> (zobj);
      Cvi* v = dynamic_cast <Cvi*> (cobj);
      if ((u != nullptr) && (v != nullptr)) return new Cblock (*u, *v);
      // check for matrices
      Cmi* mx = dynamic_cast <Cmi*> (zobj);
      Cmi* my = dynamic_cast <Cmi*> (cobj);
      if ((mx != nullptr) && (my != nullptr)) return new Cblock (*mx, *my);
      // invalid object
      throw Exception ("type-error", "invalid objects as matrix arguments");
    }
    // invalid arguments
    throw Exception ("argument-error", 
		     "invalid arguments with cblock object");
  }

  // return true if the given quark is defined

  bool Cblock::isquark (const long quark, const bool hflg) const {
    rdlock ();
    try {
      bool result = hflg ? Iterable::isquark (quark, hflg) : false;
      if (result == false) {
	result = hflg ? Cmi::isquark (quark, hflg) : false;
      }
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // operate this matrix with another object

  Object* Cblock::oper (t_oper type, Object* object) {
    Cvector* vobj = dynamic_cast <Cvector*> (object);
    Cblock*  mobj = dynamic_cast <Cblock*>  (object);
    switch (type) {
    case Object::OPER_ADD:
      if (mobj != nullptr) return new Cblock (*this + *mobj);
      break;
    case Object::OPER_SUB:
      if (mobj != nullptr) return new Cblock (*this - *mobj);
      break;
    case Object::OPER_MUL:
      if (vobj != nullptr) return new Cvector (*this * *vobj);
      if (mobj != nullptr) return new Cblock  (*this * *mobj);
      break;
    default:
      throw Exception ("matrix-error", "invalid operator with cblock",
		       Object::repr (object));
      break;
    }
    throw Exception ("type-error", "invalid operand with cblock",
		     Object::repr (object));
  }

  // apply this object with a set of arguments and a quark
  
  Object* Cblock::apply (Evaluable* zobj, Nameset* nset, const long quark,
			 Vector* argv) {
    // check the iterable method
    if (Iterable::isquark (quark, true) == true) {
      return Iterable::apply (zobj, nset, quark, argv);
    }
    // call the cmi method
    return Cmi::apply (zobj, nset, quark, argv);
  }

  // -------------------------------------------------------------------------
  // - iterator section                                                      -
  // -------------------------------------------------------------------------

  // create a new matrix iterator
  
  Cblockit::Cblockit (Cblock* mobj) {
    d_cmit = CMIT_SEQ;
    Object::iref (p_mobj = mobj);
    begin ();
  }
  
  // create a new matrix iterator by type
  
  Cblockit::Cblockit (Cblock* mobj, const t_cmit cmit) {
    d_cmit = cmit;
    Object::iref (p_mobj = mobj);
    begin ();
  }

  // destroy this matrix iterator
  
  Cblockit::~Cblockit (void) {
    Object::dref (p_mobj);
  }

  // return the class name

  String Cblockit::repr (void) const {
    return "Cblockit";
  }

  // reset the iterator to the begining

  void Cblockit::begin (void) {
    wrlock ();
    try {
      // reset indexes
      nlmove (0LL, 0LL);
      // move to the next position if needed
      if (nlgval () == 0.0) next ();
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }
 
  // reset the iterator to the end

  void Cblockit::end (void) {
    wrlock ();
    try {
      // reset indexes
      if (p_mobj == nullptr) {
	unlock ();
	return;
      }
      nlmove (p_mobj->d_rsiz-1LL, p_mobj->d_csiz-1LL);
      // move the last end if needed
      if (nlgval () == 0.0) prev ();
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the object at the current position

  Object* Cblockit::getobj (void) const {
    rdlock ();
    try {
      if (p_mobj != nullptr) p_mobj->rdlock ();
      Object* result = new Cmd (nlgrow (), nlgcol (), nlgval ());
      if (p_mobj != nullptr) p_mobj->unlock ();
      unlock ();
      return result;
    } catch (...) {
      if (p_mobj != nullptr) p_mobj->unlock ();
      unlock ();
      throw;
    }
  }

  // -------------------------------------------------------------------------
  // - iterator no lock section                                              -
  // -------------------------------------------------------------------------

  // no lock - move the iterator to the next position
  
  void Cblockit::nlnext (void) {
    switch (d_cmit) {
    case CMIT_SEQ:
      mvnext ();
      break;
    case CMIT_ROW:
      mrnext ();
      break;
    case CMIT_COL:
      mcnext ();
      break;
    }
  }

  // no lock - move the iterator to the previous position

  void Cblockit::nlprev (void) {
    // move to the previous position
    switch (d_cmit) {
    case CMIT_SEQ:
      mvprev ();
      break;
    case CMIT_ROW:
      mrprev ();
      break;
    case CMIT_COL:
      mcprev ();
      break;
    }
  }

  // no lock - return true if the iterator is at the end

  bool Cblockit::nlend (void) const {
    // check for nil
    if (p_mobj == nullptr) return true;
    // check for bounds
    return (d_mrow >= p_mobj->d_rsiz) || (d_mcol >= p_mobj->d_csiz);
  }
  
  // no lock - move the iterator to a matrix point

  void Cblockit::nlmove (const t_long row, const t_long col) {
    // check for object valid coordinates
    if ((p_mobj == nullptr) || (row < 0LL) || (col < 0LL)) return;
    // update the coordinates
    d_mrow = (row >= p_mobj->d_rsiz) ? p_mobj->d_rsiz : row;
    d_mcol = (col >= p_mobj->d_csiz) ? p_mobj->d_csiz : col;
  }

  // no lock - move the iterator to the next position
  
  void Cblockit::mvnext (void) {
    // check for nil - double security
    if (p_mobj == nullptr) return;
    // check for upper bound
    if ((d_mrow >= p_mobj->d_rsiz) && (d_mcol >= p_mobj->d_csiz)) return;
    // move to the next column
    do {
      d_mcol++;
      if (d_mcol >= p_mobj->d_csiz) {
	d_mcol = 0LL;
	d_mrow++;
      }
    } while ((nlgval () == 0.0) && (d_mrow < p_mobj->d_rsiz));
  }

  // no lock - move the iterator to the next row position

  void Cblockit::mrnext (void) {
    // check for nil - double security
    if (p_mobj == nullptr) return;
    // check for upper bound
    if ((d_mrow >= p_mobj->d_rsiz) && (d_mcol >= p_mobj->d_csiz)) return;
    // move to the next position
    do {
      d_mrow++;
    } while ((nlgval () == 0.0) && (d_mrow < p_mobj->d_rsiz));
  }

  // no lock - move the iterator to the next column position

  void Cblockit::mcnext (void) {
    // check for nil - double security
    if (p_mobj == nullptr) return;
    // check for upper bound
    if ((d_mrow >= p_mobj->d_rsiz) && (d_mcol >= p_mobj->d_csiz)) return;
    // move to the next position
    do {
      d_mcol++;
    } while ((nlgval () == 0.0) && (d_mcol < p_mobj->d_csiz));
  }

  // no lock - move the iterator to the previous position
  
  void Cblockit::mvprev (void) {
    // check for nil - double security
    if (p_mobj == nullptr) return;
    // check null first
    if ((d_mrow == 0LL) && (d_mcol == 0LL)) return;
    // move to the previous position
    do {
      d_mcol--;
      if (d_mcol < 0LL) {
	d_mcol = p_mobj->d_csiz - 1LL;
	d_mrow--;
      }
    } while ((nlgval () == 0.0) && (d_mrow >= 0));
    if (d_mcol < 0LL) d_mcol = 0LL;
    if (d_mrow < 0LL) d_mrow = 0LL;
  }

  // move the iterator to the previous row position
  
  void Cblockit::mrprev (void) {
    // check for nil - double security
    if (p_mobj == nullptr) return;
    // check null first
    if ((d_mrow == 0LL) && (d_mcol == 0LL)) return;
    // more to the previous position
    do {
      d_mrow--;
    } while ((nlgval () == 0.0) && (d_mrow >= 0));
    if (d_mrow < 0LL) d_mrow = 0LL;
    if (d_mcol < 0LL) d_mcol = 0LL;
  }

  // no lock - move the iterator to the previous column position
  
  void Cblockit::mcprev (void) {
    // check for nil - double security
    if (p_mobj == nullptr) return;
    // check null first
    if ((d_mrow == 0LL) && (d_mcol == 0LL)) return;
    // more to the previous position
    do {
      d_mcol--;
    }  while ((nlgval () == 0.0) && (d_mcol >= 0));
    if (d_mrow < 0LL) d_mrow = 0LL;
    if (d_mcol < 0LL) d_mcol = 0LL;
  }

  // no lock - get the row coordinate at the iterator position (no lock)
  
  t_long Cblockit::nlgrow (void) const {
    // check for nil first
    if (p_mobj == nullptr) return 0LL;
    return d_mrow;
  }

  // no lock - get the column coordinate at the iterator position
  
  t_long Cblockit::nlgcol (void) const {
    // check for nil first
    if (p_mobj == nullptr) return 0LL;
    return d_mcol;
  }

  // no lock - set the value at the current position

  void Cblockit::nlsval (const Complex& val)  {
    // check for nil first
    if (p_mobj == nullptr) return;
    // check for bound
    if ((d_mrow >= p_mobj->d_rsiz) || (d_mcol >= p_mobj->d_csiz)) return;
    p_mobj->nlset (d_mrow, d_mcol, val);
  }

  // no lock - get the value at the current position (no lock)

  Complex Cblockit::nlgval (void) const {
    // check for nil first
    if (p_mobj == nullptr) return 0LL;
    // check for bound
    if ((d_mrow >= p_mobj->d_rsiz) || (d_mcol >= p_mobj->d_csiz)) return 0.0;
    return p_mobj->nlget (d_mrow, d_mcol);
  }
}
