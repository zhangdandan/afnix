// ---------------------------------------------------------------------------
// - Rtrace2.cpp                                                             -
// - afnix:mth module - real trace 2 implementation                          -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Real.hpp"
#include "Mthsid.hxx"
#include "Vector.hpp"
#include "Rtrace2.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"

namespace afnix {
  
  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a default trace

  Rtrace2::Rtrace2 (void) : Rtrace (2) {
    clear ();
  }
  
  // create a trace by component

  Rtrace2::Rtrace2 (const t_real x, const t_real y) : Rtrace (2) {
    clear ();
    p_vtab[0] = x;
    p_vtab[1] = y;
  }

  // copy construct this trace

  Rtrace2::Rtrace2 (const Rtrace2& that) {
    that.rdlock ();
    try {
      Rtrace::operator = (that);
      that.unlock ();
    } catch (...) {
      that.unlock ();
      throw;
    }  
  }

  // assign a trace to this one

  Rtrace2& Rtrace2::operator = (const Rtrace2& that) {
    // check for self-assignation
    if (this == &that) return *this;
    // lock and assign
    wrlock ();
    that.rdlock ();
    try {
      Rtrace::operator = (that);
      unlock ();
      that.unlock ();
      return *this;
    } catch (...) {
      unlock ();
      that.unlock ();
      throw;
    }
  }

  // return the class name

  String Rtrace2::repr (void) const {
    return "Rtrace2";
  }

  // return a clone of this object

  Object* Rtrace2::clone (void) const {
    return new Rtrace2 (*this);
  }

  // return the serial did
  
  t_word Rtrace2::getdid (void) const {
    return SRL_DEOD_MTH;
  }
  
  // return the serial sid

  t_word Rtrace2::getsid (void) const {
    return SRL_TRC2_SID;
  }

  // serialize this trace

  void Rtrace2::wrstream (OutputStream& os) const {
    rdlock ();
    try {
      // serialize the trace
      Rtrace::wrstream (os);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // deserialize this trace

  void Rtrace2::rdstream (InputStream& is) {
    wrlock ();
    try {
      // deserialize the trace
      Rtrace::rdstream (is);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // get the trace x component

  t_real Rtrace2::getx (void) const {
    rdlock ();
    try {
      t_real result = p_vtab[0];
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // get the trace y component

  t_real Rtrace2::gety (void) const {
    rdlock ();
    try {
      t_real result = p_vtab[1];
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 2;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_GETX = zone.intern ("get-x");
  static const long QUARK_GETY = zone.intern ("get-y");

  // create a new object in a generic way

  Object* Rtrace2::mknew (Vector* argv) {
    long argc = (argv == nullptr) ? 0 : argv->length ();
    // check for 0 argument
    if (argc == 0) return new Rtrace2;
    // check for 2 arguments
    if (argc == 2) {
      t_real x = argv->getrint (0);
      t_real y = argv->getrint (1);
      return new Rtrace2 (x, y);
    }
    throw Exception ("argument-error", "invalid arguments with trace 2"); 
  }

  // return true if the given quark is defined
  
  bool Rtrace2::isquark (const long quark, const bool hflg) const {
    rdlock ();
    try {
      if (zone.exists (quark) == true) {
	unlock ();
	return true;
      }
      bool result = hflg ? Rtrace::isquark (quark, hflg) : false;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // apply this object with a set of arguments and a quark

  Object* Rtrace2::apply (Evaluable* zobj, Nameset* nset, const long quark,
			  Vector* argv) {
    // get the number of arguments
    long argc = (argv == nullptr) ? 0 : argv->length ();
    
    // dispatch 0 argument
    if (argc == 0) {
      if (quark == QUARK_GETX) return new Real (getx ());
      if (quark == QUARK_GETY) return new Real (gety ());
    }
    // call the trace method
    return Rtrace::apply (zobj, nset, quark, argv);
  }
}
