// ---------------------------------------------------------------------------
// - Cvi.cpp                                                                 -
// - afnix:mth module - complex vector interface implementation              -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Cvi.hpp"
#include "Math.hpp"
#include "Real.hpp"
#include "Mthsid.hxx"
#include "Vector.hpp"
#include "Boolean.hpp"
#include "Utility.hpp"
#include "Algebra.hpp"
#include "Evaluable.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"
#include "OutputStream.hpp"
 
namespace afnix {

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a vector by size

  Cvi::Cvi (const t_long size) : Avi (size) {
  }
  
  // copy construct this vector

  Cvi::Cvi (const Cvi& that) {
    that.rdlock ();
    try {
      Avi::operator = (that);
      that.unlock ();
    } catch (...) {
      that.unlock ();
      throw;
    }
  }
  
  // copy move this vector

  Cvi::Cvi (Cvi&& that) noexcept {
    that.wrlock ();
    try {
      Avi::operator = (static_cast<Avi&&>(that));
      that.unlock ();
    } catch (...) {
      d_size = 0L;
      that.unlock ();
    }
  }

  // assign a vector to this one

  Cvi& Cvi::operator = (const Cvi& that) {
    // check for self-assignation
    if (this == &that) return *this;
    // lock and assign
    wrlock ();
    that.rdlock ();
    try {
      Avi::operator = (that);
      that.unlock ();
      unlock ();
      return *this;
    } catch (...) {
      unlock ();
      that.unlock ();
      throw;
    }
  }
  
  // move a vector into this one

  Cvi& Cvi::operator = (Cvi&& that) noexcept {
    // check for self-move
    if (this == &that) return *this;
    // lock and move
    wrlock ();
    that.wrlock ();
    try {
      Avi::operator = (static_cast<Avi&&>(that));
      unlock ();
      that.unlock ();
    } catch (...) {
      d_size = 0L;
      unlock ();
      that.unlock ();
    }
    return *this;
  }
  
  // serialize this object
  
  void Cvi::wrstream (OutputStream& os) const {
    rdlock ();
    try {
      // write the vector size/mode
      Serial::wrlong (d_size, os);
      // write the vector data
      for (long k = 0; k < d_size; k++) {
	// get the vector value
	Complex val = nlget (k);
	if (val.iszero () == true) continue;
	// write by position
	Serial::wrlong (k,   os);
	val.wrstream (os);
      }
      // write marker for end
      Serial::wrlong (-1, os);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // deserialize this object

  void Cvi::rdstream (InputStream& is) {
    wrlock ();
    try {
      // reset the vector
      reset ();
      // get the vector size/mode
      long size = Serial::rdlong (is);
      // resize the vector
      resize (size);
      // get the vector data by position
      for (long k = 0; k < size; k++) {
	long idx = Serial::rdlong (is);
	// check for marker
	if (idx == -1) {
	  unlock ();
	  return;
	}
	Complex val; val.rdstream (is);
	nlset (idx, val);
      }
      // get the remaining marker
      if (Serial::rdlong (is) != -1) {
	throw Exception ("cvi-error", "inconsistent serialized vector");
      }
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // reset this vector

  void Cvi::reset (void) {
    wrlock ();
    try {
      d_size = 0LL;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // clear this vector

  void Cvi::clear (void) {
    wrlock ();
    try {
      for (t_long i = 0; i < d_size; i++) nlset (i, 0.0);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // check if this vector is nil

  bool Cvi::isnil (void) const {
    rdlock ();
    try {
      bool result = Algebra::isnil (*this);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // convert to a vector of literal

  Vector Cvi::tovector (void) const {
    rdlock ();
    try {
      Vector result;
      for (t_long k = 0LL; k < d_size; k++) {
	result.add (new Complex (nlget (k)));
      }
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the vector format

  String Cvi::tofrmt (void) const {
    rdlock ();
    try {
      String result = Complex::CV_NUM_FRMT;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // compare two vectors

  bool Cvi::operator == (const Cvi& x) const {
    rdlock ();
    x.rdlock ();
    try {
      bool result = Algebra::eql (*this, x);
      unlock ();
      x.unlock ();
      return result;
    } catch (...) {
      unlock ();
      x.unlock ();
      throw;
    }
  }

  // compare two vectors

  bool Cvi::operator != (const Cvi& x) const {
    return !(*this == x);
  }

  // add this vector by a scalar

  Cvi& Cvi::operator += (const Complex& s) {
    return this->add (*this, s);
  }

  // add this vector with a vector

  Cvi& Cvi::operator += (const Cvi& x) {
    return this->aeq (x);
  }

  // substract this vector by a scalar

  Cvi& Cvi::operator -= (const Complex& s) {
    return this->sub (*this, s);
  }

  // multiply this vector by a scalar

  Cvi& Cvi::operator *= (const Complex& s) {
    return this->mul (*this, s);
  }
  
  // compute the vector dot product

  Complex Cvi::operator ^ (const Cvi& x) const {
    rdlock ();
    x.rdlock ();
    try {
      Complex result = Algebra::hdot (*this, x);
      unlock ();
      x.unlock ();
      return result;
    } catch (...) {
      unlock ();
      x.unlock ();
      throw;
    }
  }

  // copy a vector into this one

  Cvi& Cvi::cpy (const Cvi& x) {
    wrlock ();
    try {
      Algebra::cpy (*this, x);
      unlock ();
      return *this;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // compare a vector value by position

  bool Cvi::cmp (const t_long pos, const Complex& val) const {
    rdlock ();
    try {
      if ((pos < 0) || (pos >= d_size)) {
	throw Exception ("index-error", "invalid vector position");
      }
      Complex z = nlget (pos);
      bool result = z.cmp (val);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // compare two vectors by precision

  bool Cvi::cmp (const Cvi& x) const {
    rdlock ();
    x.rdlock ();
    try {
      bool result = Algebra::cmp (*this, x);
      unlock ();
      x.unlock ();
      return result;
    } catch (...) {
      unlock ();
      x.unlock ();
      throw;
    }
  }

  // compute the vector norm

  t_real Cvi::norm (void) const {
    rdlock ();
    try {
      t_real result = Algebra::norm (*this);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set a vector by value

  void Cvi::set (const Complex& val) {
    wrlock ();
    try {
      for (t_long i = 0; i < d_size; i++) nlset (i, val);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set a vector by position

  void Cvi::set (const t_long pos, const Complex& val) {
    wrlock ();
    try {
      if ((pos < 0) || (pos >= d_size)) {
	throw Exception ("index-error", "invalid vector position");
      }
      nlset (pos, val);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get a vector by position

  Complex Cvi::get (const t_long pos) const {
    rdlock ();
    try {
      if ((pos < 0) || (pos >= d_size)) {
	throw Exception ("index-error", "invalid vector position");
      }
      Complex result = nlget (pos);
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // add a vector with a scalar

  Cvi& Cvi::add (const Cvi& x, const Complex& s) {
    wrlock ();
    try {
      Algebra::add (*this, x, s);
      unlock ();
      return *this;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // add a vector with another one

  Cvi& Cvi::add (const Cvi& x, const Cvi& y) {
    wrlock ();
    try {
      Algebra::add (*this, x, y);
      unlock ();
      return *this;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // add a vector with another scaled one

  Cvi& Cvi::add (const Cvi& x, const Cvi& y, const Complex& s) {
    wrlock ();
    try {
      Algebra::add (*this, x, y, s);
      unlock ();
      return *this;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // substract a vector with a scalar

  Cvi& Cvi::sub (const Cvi& x, const Complex& s) {
    wrlock ();
    try {
      Algebra::sub (*this, x, s);
      unlock ();
      return *this;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // substract a vector with another one

  Cvi& Cvi::sub (const Cvi& x, const Cvi& y) {
    wrlock ();
    try {
      Algebra::sub (*this, x, y);
      unlock ();
      return *this;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // multiply a vector with a scaled factor

  Cvi& Cvi::mul (const Cvi& x, const Complex& s) {
    wrlock ();
    try {
      Algebra::mul (*this, x, s);
      unlock ();
      return *this;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // multiply a vector with another one

  Cvi& Cvi::mul (const Cvi& x, const Cvi& y) {
    wrlock ();
    try {
      Algebra::mul (*this, x, y);
      unlock ();
      return *this;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // divide a vector with another one

  Cvi& Cvi::div (const Cvi& x, const Cvi& y) {
    wrlock ();
    try {
      Algebra::div (*this, x, y);
      unlock ();
      return *this;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // add equal with a vector

  Cvi& Cvi::aeq (const Cvi& x) {
    wrlock ();
    try {
      Algebra::aeq (*this, x);
      unlock ();
      return *this;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // add equal with a scaled vector

  Cvi& Cvi::aeq (const Cvi& x, const Complex& s) {
    wrlock ();
    try {
      Algebra::aeq (*this, x, s);
      unlock ();
      return *this;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // rescale equal with a vector

  Cvi& Cvi::req (const Cvi& x, const Complex& s) {
    wrlock ();
    try {
      Algebra::req (*this, x, s);
      unlock ();
      return *this;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // normalize this vector

  Cvi& Cvi::normalize (void) {
    wrlock ();
    try {
      Algebra::normalize (*this, *this);
      unlock ();
      return *this;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // permutate this vector
  
  Cvi* Cvi::permutate (const Cpi& p) const {
    throw Exception ("cvi-error", "unimplemented vector permutate");
  }

  // reverse permutate this vector
  
  Cvi* Cvi::reverse (const Cpi& p) const {
    throw Exception ("cvi-error", "unimplemented vector reverse permutate");
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 13;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_EQL       = zone.intern ("==");
  static const long QUARK_NEQ       = zone.intern ("!=");
  static const long QUARK_QEQ       = zone.intern ("?=");
  static const long QUARK_AEQ       = zone.intern ("+=");
  static const long QUARK_SEQ       = zone.intern ("-=");
  static const long QUARK_MEQ       = zone.intern ("*=");
  static const long QUARK_DEQ       = zone.intern ("/=");
  static const long QUARK_SET       = zone.intern ("set");
  static const long QUARK_GET       = zone.intern ("get");
  static const long QUARK_DOT       = zone.intern ("dot");
  static const long QUARK_NORM      = zone.intern ("norm");
  static const long QUARK_REVERSE   = zone.intern ("reverse");
  static const long QUARK_PERMUTATE = zone.intern ("permutate");

  // return true if the given quark is defined

  bool Cvi::isquark (const long quark, const bool hflg) const {
    rdlock ();
    try {
      if (zone.exists (quark) == true){
	unlock ();
	return true;
      }
      bool result = hflg ? Avi::isquark (quark, hflg) : false;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // operate this object with another object

  Object* Cvi::oper (t_oper type, Object* object) {
    Cvi* vobj = dynamic_cast <Cvi*> (object);
    switch (type) {
    case Object::OPER_EQL:
      if (vobj != nullptr) return new Boolean (*this == *vobj);
      break;
    case Object::OPER_QEQ:
      if (vobj != nullptr) return new Boolean (cmp(*vobj));
      break;
    case Object::OPER_NEQ:
      if (vobj != nullptr) return new Boolean (*this != *vobj);
      break;
    default:
      break;
    }
    throw Exception ("type-error", "invalid operand with real vector",
		     Object::repr (object));
  }
  
  // apply this object with a set of arguments and a quark
  
  Object* Cvi::apply (Evaluable* zobj, Nameset* nset, const long quark,
                      Vector* argv) {
    // get the number of arguments
    long argc = (argv == nullptr) ? 0 : argv->length ();

    // dispatch 0 argument
    if (argc == 0) {
      if (quark == QUARK_NORM) return new Real (norm ());
    }
    // dispatch 1 argument
    if (argc == 1) {
      if (quark == QUARK_EQL) return oper (Object::OPER_EQL, argv->get (0));
      if (quark == QUARK_QEQ) return oper (Object::OPER_QEQ, argv->get (0));
      if (quark == QUARK_NEQ) return oper (Object::OPER_NEQ, argv->get (0));
      if (quark == QUARK_GET) {
        t_long pos = argv->getlong (0);
        return new Complex (get (pos));
      }
      if (quark == QUARK_DOT) {
	Object* obj = argv->get (0);
	Cvi*    rvo = dynamic_cast <Cvi*> (obj);
	if (rvo == nullptr) {
	  throw Exception ("type-error", "invalid object for dot product",
			   Object::repr (obj));
	}
	return new Complex (*this ^ *rvo);
      }
      if (quark == QUARK_AEQ) {
	wrlock ();
	try {
	  Object* obj = argv->get (0);
	  // check for a real
	  auto robj = dynamic_cast <Real*> (obj);
	  if (robj != nullptr) {
	    *this += Complex(robj->toreal ());
	    zobj->post (this);
	    unlock ();
	    return this;
	  }
	  // check for a complex
	  auto cobj = dynamic_cast <Complex*> (obj);
	  if (cobj != nullptr) {
	    *this += *cobj;
	    zobj->post (this);
	    unlock ();
	    return this;
	  }
	  // check for a vector
	  auto vobj = dynamic_cast <Cvi*> (obj);
	  if (vobj != nullptr) {
	    *this += (*vobj);
	    zobj->post (this);
	    unlock ();
	    return this;
	  }
	  throw Exception ("type-error", "invalid object for vector aeq",
			   Object::repr (obj));
	} catch (...) {
	  unlock ();
	  throw;
	}
      }
      if (quark == QUARK_SEQ) {
	wrlock ();
	try {
	  Object* obj = argv->get (0);
	  //  check for a real
	  auto robj = dynamic_cast <Real*> (obj);
	  if (robj != nullptr) {
	    *this -= robj->toreal ();
	    zobj->post (this);
	    unlock ();
	    return this;
	  }
	  //  check for a complex
	  auto cobj = dynamic_cast <Complex*> (obj);
	  if (cobj != nullptr) {
	    *this -= *cobj;
	    zobj->post (this);
	    unlock ();
	    return this;
	  }
	  throw Exception ("type-error", "invalid object for vector seq",
			   Object::repr (obj));
	} catch (...) {
	  unlock ();
	  throw;
	}
      }
      if (quark == QUARK_MEQ) {
	wrlock ();
	try {
	  Object* obj = argv->get (0);
	  // check for a real
	  auto robj = dynamic_cast <Real*> (obj);
	  if (robj != nullptr) {
	    *this *= robj->toreal ();
	    zobj->post (this);
	    unlock ();
	    return this;
	  }
	  // check for a complex
	  auto cobj = dynamic_cast <Complex*> (obj);
	  if (cobj != nullptr) {
	    *this *= *cobj;
	    zobj->post (this);
	    unlock ();
	    return this;
	  }
	  throw Exception ("type-error", "invalid object for vector meq",
			   Object::repr (obj));
	} catch (...) {
	  unlock ();
	  throw;
	}
      }
      if (quark == QUARK_DEQ) {
	wrlock ();
	try {
	  Object* obj = argv->get (0);
	  // check for a real
	  auto robj = dynamic_cast <Real*> (obj);
	  if (robj != nullptr) {
	    *this *= (1.0 / robj->toreal());
	    zobj->post (this);
	    unlock ();
	    return this;
	  }
	  // check for a complex
	  auto cobj = dynamic_cast <Complex*> (obj);
	  if (cobj != nullptr) {
	    *this *= (1.0 / *cobj);
	    zobj->post (this);
	    unlock ();
	    return this;
	  }
	  throw Exception ("type-error", "invalid object for vector deq",
			   Object::repr (obj));
	} catch (...) {
	  unlock ();
	  throw;
	}
      }
      if (quark == QUARK_PERMUTATE) {
	Object* obj = argv->get (0);
	Cpi* p = dynamic_cast <Cpi*> (obj);
	if (p == nullptr) {
	  throw Exception ("type-error", "invalid object with permutate",
			   Object::repr (obj));
	}
	return permutate (*p);
      }
      if (quark == QUARK_REVERSE) {
	Object* obj = argv->get (0);
	Cpi* p = dynamic_cast <Cpi*> (obj);
	if (p == nullptr) {
	  throw Exception ("type-error", "invalid object with permutate",
			   Object::repr (obj));
	}
	return reverse (*p);
      }
    }
    // dispatch 2 arguments
    if (argc == 2) {
      if (quark == QUARK_SET) {
        t_long pos = argv->getlong (0);
	Object* obj = argv->get (1);
	// check for real
	auto robj = dynamic_cast <Real*> (obj);
	if (robj != nullptr) {
	  set (pos, robj->toreal ());
	  return nullptr;
	}
	// check for complex
	auto cobj = dynamic_cast <Complex*> (obj);
	if (cobj != nullptr) {
	  set (pos, *cobj);
	  return nullptr;
	}
	throw Exception ("type-error", "invalid object for vector set",
			 Object::repr (obj));
      }
      if (quark == QUARK_QEQ) {
	t_long  pos = argv->getlong (0);
	Object* obj = argv->get (1);
	// check for a real
	auto robj = dynamic_cast <Real*> (obj);
	if (robj != nullptr) {
	  return new Boolean (cmp (pos, robj->toreal()));
	}
	// check for a complex
	auto cobj = dynamic_cast <Complex*> (obj);
	if (cobj != nullptr) {
	  return new Boolean (cmp (pos, *cobj));
	}
	throw Exception ("type-error", "invalid object for vector qeq",
			 Object::repr (obj));
      }
    }
    // call the abstract vector
    return Avi::apply (zobj, nset, quark, argv);
  }
}

