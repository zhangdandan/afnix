// ---------------------------------------------------------------------------
// - Rtrace4.hpp                                                             -
// - afnix:mth module - real trace 4 definitions                             -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_RTRACE4_HPP
#define  AFNIX_RTRACE4_HPP

#ifndef  AFNIX_RTRACE_HPP
#include "Rtrace.hpp"
#endif

namespace afnix {
  
  /// The Rtrace4 class is a 4 dimensional trace class for the afnix math
  /// module. A 4 dimensional trace is defined by its (x,y,z,w) components in
  /// a base choosen by the system.
  /// @author amaury darsch

  class Rtrace4 : public Rtrace {
  public:
    /// create a default trace
    Rtrace4 (void);

    /// create a trace by components
    /// @param x the x component
    /// @param y the y component
    /// @param z the z component
    /// @param w the z component
    Rtrace4 (const t_real x, const t_real y, const t_real z, const t_real w);

    /// copy construct this trace
    /// @param that the trace to copy
    Rtrace4 (const Rtrace4& that);

    /// assign a trace to this one
    /// @param that the trace to assign
    Rtrace4& operator = (const Rtrace4& that);
    
    /// @return the class name
    String repr (void) const override;
    
    /// @return a clone of this object
    Object* clone (void) const override;

    /// @return the serial did
    t_word getdid (void) const override;

    /// @return the serial sid
    t_word getsid (void) const override;

    /// serialize this material
    /// @param os the output stream
    void wrstream (OutputStream& os) const override;

    /// deserialize this material
    /// @param is the input stream
    void rdstream (InputStream& os) override;
    
    /// @return the trace x component
    virtual t_real getx (void) const;

    /// @return the trace y component
    virtual t_real gety (void) const;
    
    /// @return the trace z component
    virtual t_real getz (void) const;
    
    /// @return the trace w component
    virtual t_real getw (void) const;
    
  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);

    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const override;

    /// apply this object with a set of arguments and a quark
    /// @param zobj  the current evaluable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Evaluable* zobj, Nameset* nset, const long quark,
                   Vector* argv) override;
  };
}

#endif
