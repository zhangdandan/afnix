// ---------------------------------------------------------------------------
// - Cti.hpp                                                                 -
// - afnix:mth module - complex trace interface definitions                  -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_CTI_HPP
#define  AFNIX_CTI_HPP

#ifndef  AFNIX_ATI_HPP
#include "Ati.hpp"
#endif

#ifndef  AFNIX_CPI_HPP
#include "Cpi.hpp"
#endif

#ifndef  AFNIX_COMPLEX_HPP
#include "Complex.hpp"
#endif

namespace afnix {

  /// This Cti class is an abstract class that models the behatior of a
  /// complex based trace. The class extends the abstract trace interface
  /// with specific complex methods.
  /// @author amaury darsch

  class Cti : public Ati {
  public:
    /// create a null trace
    Cti (void) =default;

    /// create a trace by size
    /// @param size the trace size
    Cti (const t_long size);

    /// copy construct this trace
    /// @param that the trace to copy
    Cti (const Cti& that);

    /// copy move this trace
    /// @param that the trace to move
    Cti (Cti&& that) noexcept;

    /// assign a trace into this one
    /// @param that the trace to assign
    Cti& operator = (const Cti& that);

    /// move a trace into this one
    /// @param that the trace to move
    Cti& operator = (Cti&& that) noexcept;

    /// serialize this object
    /// @param os the output stream
    void wrstream (OutputStream& os) const override;

    /// deserialize this object
    /// @param is the input stream
    void rdstream (InputStream& os) override;

    /// reset this trace
    void reset (void) override;

    /// clear this trace
    void clear (void) override;

    /// @return true if the trace is nil
    bool isnil (void) const override;

    /// @return a trace of literals
    Vector tovector (void) const override;

    /// @return the trace format
    String tofrmt (void) const override;

    /// compare two traces
    /// @param  x the trace argument
    /// @return true if they are equals
    virtual bool operator == (const Cti& x) const;

    /// compare two traces
    /// @param  x the trace argument
    /// @return true if they are not equals
    virtual bool operator != (const Cti& x) const;

    /// add a trace by a scalar
    /// @param z the scalar value
    virtual Cti& operator += (const Complex& z);

    /// add a trace with a trace
    /// @param x the trace argument
    virtual Cti& operator += (const Cti& x);

    /// substract a trace by a scalar
    /// @param z the scalar value
    virtual Cti& operator -= (const Complex& z);

    /// multiply a trace by a scalar
    /// @param z the scalar value
    virtual Cti& operator *= (const Complex& z);

    /// copy a trace into this one
    /// @param x the trace to copy
    virtual Cti& cpy (const Cti& x);

    /// compare a trace value
    /// @param pos the trace position
    /// @param val the value to compare
    virtual bool cmp (const t_long pos, const Complex& val) const;

    /// compare two traces upto a precision
    /// @param x the trace argument
    virtual bool cmp (const Cti& x) const;

    /// set an identity trace
    virtual void eye (void);

    /// set a trace by value
    /// @param val the value to set
    virtual void set (const Complex& val);

    /// set a trace by position
    /// @param pos the trace position
    /// @param val the value to set
    virtual void set (const t_long pos, const Complex& val);

    /// get a trace value by position
    /// @param pos the trace position
    virtual Complex get (const t_long pos) const;

    /// add a trace with a scalar
    /// @param x the trace argument
    /// @param z the scalar factor
    virtual Cti& add (const Cti& x, const Complex& z);

    /// add a trace with another one
    /// @param x the trace argument
    /// @param y the trace argument
    virtual Cti& add (const Cti& x, const Cti& y);

    /// add a trace with another scaled one
    /// @param x the trace argument
    /// @param y the trace argument
    /// @param z the scalar factor
    virtual Cti& add (const Cti& x, const Cti& y, const Complex& z);

    /// substract a trace with a scalar
    /// @param x the trace argument
    /// @param z the scalar factor
    virtual Cti& sub (const Cti& x, const Complex& z);

    /// substract a trace with another one
    /// @param x the trace argument
    /// @param y the trace argument
    virtual Cti& sub (const Cti& x, const Cti& y);

    /// multiply a trace with a scaled trace
    /// @param x the trace to multiply
    /// @param z the scaling factor
    virtual Cti& mul (const Cti& x, const Complex& z);

    /// multiply a trace with another one
    /// @param x the trace argument
    /// @param y the trace argument
    virtual Cti& mul (const Cti& x, const Cti& y);

    /// divide a trace with another one
    /// @param x the trace argument
    /// @param y the trace argument
    virtual Cti& div (const Cti& x, const Cti& y);

    /// add equal with a trace
    /// @param x the trace to add
    virtual Cti& aeq (const Cti& x);

    /// add equal with a scaled trace
    /// @param x the trace to add
    /// @param z the scaling factor
    virtual Cti& aeq (const Cti& x, const Complex& z);

    /// rescale equal with a trace
    /// @param x the trace to add
    /// @param z the scaling factor
    virtual Cti& req (const Cti& x, const Complex& s);

    /// permutate this trace
    /// @param p the permutation object
    virtual Cti* permutate (const Cpi& p) const;

    /// reverse this trace permutation
    /// @param p the permutation object
    virtual Cti* reverse (const Cpi& p) const;

  public:
    /// no lock - set a trace by position
    /// @param pos the trace position
    /// @param val the value to set
    virtual void nlset (const t_long pos, const Complex& val) =0;

    /// no lock - get a trace value by position
    /// @param pos the trace position
    virtual Complex nlget (const t_long pos) const =0;

  public:
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const override;

    /// operate this object with another object
    /// @param type   the operator type
    /// @param object the operand object
    Object* oper (t_oper type, Object* object) override;

    /// apply this object with a set of arguments and a quark
    /// @param zobj  the current evaluable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Evaluable* zobj, Nameset* nset, const long quark,
                   Vector* argv) override;
  };
}

#endif
