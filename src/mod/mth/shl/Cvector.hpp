// ---------------------------------------------------------------------------
// - Cvector.hpp                                                             -
// - afnix:mth module - complex vector definitions                           -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_CVECTOR_HPP
#define  AFNIX_CVECTOR_HPP

#ifndef  AFNIX_CVI_HPP
#include "Cvi.hpp"
#endif
 
#ifndef  AFNIX_VIEWABLE_HPP
#include "Viewable.hpp"
#endif
 
namespace afnix {

  /// This Cvector class is the default implementation of the complex vector
  /// interface. Internally, the vector is represented as an array of real
  /// pairs.
  /// @author amaury darsch

  class Cvector : public Cvi, public Viewable {
  public:
    /// add a vector with a scalar
    /// @param x the vector argument
    /// @param s the scalar argument
    friend Cvector operator + (const Cvector& x, const Complex& s);

    /// add a vector with another one
    /// @param x the vector argument
    /// @param y the vector argument
    friend Cvector operator + (const Cvector& x, const Cvector& y);

    /// substract a vector with a scalar
    /// @param x the vector argument
    /// @param s the scalar argument
    friend Cvector operator - (const Cvector& x, const Complex& s);

    /// substract a vector with another one
    /// @param x the vector argument
    /// @param y the vector argument
    friend Cvector operator - (const Cvector& x, const Cvector& y);

    /// multiply a vector with a scalar
    /// @param x the vector argument
    /// @param s the scalar argument
    friend Cvector operator * (const Cvector& x, const Complex& s);

    /// divide a vector with a scalar
    /// @param x the vector argument
    /// @param s the scalar argument
    friend Cvector operator / (const Cvector& x, const Complex& s);

  protected:
    /// the vector elements
    t_real* p_vtab;

  public:
    /// create a null vector
    Cvector (void);

    /// create a vector by size
    /// @param size the vector size
    Cvector (const t_long size);

    /// copy construct this vector
    /// @param that the object to copy
    Cvector (const Cvector& that);

    /// destroy this vector
    ~Cvector (void);

    /// assign a vector to this one
    /// @param that the object to assign
    Cvector& operator = (const Cvector& that);

    /// compare two vectors
    /// @param  x the vector argument
    /// @return true if they are equals
    bool operator == (const Cvi& x) const override;

    /// compute the vector dot product
    /// @param x the vector argument
    Complex operator ^ (const Cvi& x) const override;
    
    /// @return the class name
    String repr (void) const override;

    /// @return a clone of this object
    Object* clone (void) const override;

    /// @return the serial did
    t_word getdid (void) const override;

    /// @return the serial sid
    t_word getsid (void) const override;
    
    /// resize this vector
    /// @param size the new vector size
    void resize (const t_long size) override;

    /// reset this vector
    void reset (void) override;

    /// clear this vector
    void clear (void) override;

    /// preset this vector
    void preset (void) override;

    /// @return the vector norm
    t_real norm (void) const override;

    /// copy a vector into this one
    /// @param x the vector to copy
    Cvi& cpy (const Cvi& x) override;

    /// add a vector with a scalar
    /// @param x the vector argument
    /// @param s the scalar factor
    Cvi& add (const Cvi& x, const Complex& s) override;

    /// add a vector with another one
    /// @param x the vector argument
    /// @param y the vector argument
    Cvi& add (const Cvi& x, const Cvi& y) override;

    /// add a vector with another scaled one
    /// @param x the vector argument
    /// @param y the vector argument
    /// @param s the scalar factor
    Cvi& add (const Cvi& x, const Cvi& y, const Complex& s) override;

    /// substract a vector with a scalar
    /// @param x the vector argument
    /// @param s the scalar factor
    Cvi& sub (const Cvi& x, const Complex& s) override;

    /// substract a vector with another one
    /// @param x the vector argument
    /// @param y the vector argument
    Cvi& sub (const Cvi& x, const Cvi& y) override;

    /// multiply a vector with a scaled vector
    /// @param x the vector to multiply
    /// @param s the scaling factor
    Cvi& mul (const Cvi& x, const Complex& s) override;

    /// multiply a vector with another one
    /// @param x the vector argument
    /// @param y the vector argument
    Cvi& mul (const Cvi& x, const Cvi& y) override;

    /// divide a vector with another one
    /// @param x the vector argument
    /// @param y the vector argument
    Cvi& div (const Cvi& x, const Cvi& y) override;

    /// add equal with a vector
    /// @param x the vector to add
    Cvi& aeq (const Cvi& x) override;

    /// add equal with a scaled vector
    /// @param x the vector to add
    /// @param s the scaling factor
    Cvi& aeq (const Cvi& x, const Complex& s) override;

    /// resize equal with a scaled vector
    /// @param x the vector to add
    /// @param s the scaling factor
    Cvi& req (const Cvi& x, const Complex& s) override;

    /// permutate this vector
    /// @param p the permutation object
    Cvi* permutate (const Cpi& p) const override;
    
    /// reverse this vector permutation
    /// @param p the permutation object
    Cvi* reverse (const Cpi& p) const override;

    /// @return the viewable size
    long tosize (void) const override;

    /// @return the viewable data
    t_byte* tobyte (void) override;
    
    /// @return the viewable data
    const t_byte* tobyte (void) const override;
    
  public:
    /// no lock - set a vector by position
    /// @param pos the vector position
    /// @param val the value to set
    void nlset (const t_long pos, const Complex& val) override;

    /// no lock - get a vector value by position
    /// @param pos the vector position
    Complex nlget (const t_long pos) const override;

  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);

    /// operate this object with another object
    /// @param type   the operator type
    /// @param object the operand object
    Object* oper (t_oper type, Object* object) override;
  };
}

#endif
