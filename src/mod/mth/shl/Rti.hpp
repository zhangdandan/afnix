// ---------------------------------------------------------------------------
// - Rti.hpp                                                                 -
// - afnix:mth module - real trace interface definitions                     -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_RTI_HPP
#define  AFNIX_RTI_HPP

#ifndef  AFNIX_ATI_HPP
#include "Ati.hpp"
#endif

#ifndef  AFNIX_CPI_HPP
#include "Cpi.hpp"
#endif

namespace afnix {

  /// This Rti class is an abstract class that models the behatior of a
  /// real based trace. The class extends the abstract trace interface with
  /// specific real methods.
  /// @author amaury darsch

  class Rti : public Ati {
  public:
    /// create a null trace
    Rti (void) =default;

    /// create a trace by size
    /// @param size the trace size
    Rti (const t_long size);

    /// copy construct this trace
    /// @param that the trace to copy
    Rti (const Rti& that);

    /// copy move this trace
    /// @param that the trace to move
    Rti (Rti&& that) noexcept;

    /// assign a trace into this one
    /// @param that the trace to assign
    Rti& operator = (const Rti& that);

    /// move a trace into this one
    /// @param that the trace to move
    Rti& operator = (Rti&& that) noexcept;

    /// serialize this object
    /// @param os the output stream
    void wrstream (OutputStream& os) const override;

    /// deserialize this object
    /// @param is the input stream
    void rdstream (InputStream& os) override;

    /// reset this trace
    void reset (void) override;

    /// clear this trace
    void clear (void) override;

    /// @return true if the trace is nil
    bool isnil (void) const override;

    /// @return a vector of literals
    Vector tovector (void) const override;

    /// @return the trace format
    String tofrmt (void) const override;

    /// compare two traces
    /// @param  x the trace argument
    /// @return true if they are equals
    virtual bool operator == (const Rti& x) const;

    /// compare two traces
    /// @param  x the trace argument
    /// @return true if they are not equals
    virtual bool operator != (const Rti& x) const;

    /// add a trace by a scalar
    /// @param s the scalar value
    virtual Rti& operator += (const t_real s);

    /// add a trace with a trace
    /// @param x the trace argument
    virtual Rti& operator += (const Rti& x);

    /// substract a trace by a scalar
    /// @param s the scalar value
    virtual Rti& operator -= (const t_real s);

    /// multiply a trace by a scalar
    /// @param s the scalar value
    virtual Rti& operator *= (const t_real s);

    /// copy a trace into this one
    /// @param x the trace to copy
    virtual Rti& cpy (const Rti& x);

    /// compare a trace value
    /// @param pos the trace position
    /// @param val the value to compare
    virtual bool cmp (const t_long pos, const t_real val) const;

    /// compare two traces upto a precision
    /// @param x the trace argument
    virtual bool cmp (const Rti& x) const;

    /// set an identity trace
    virtual void eye (void);
    
    /// set a trace by value
    /// @param val the value to set
    virtual void set (const t_real val);

    /// set a trace by position
    /// @param pos the trace position
    /// @param val the value to set
    virtual void set (const t_long pos, const t_real val);

    /// get a trace value by position
    /// @param pos the trace position
    virtual t_real get (const t_long pos) const;

    /// add a trace with a scalar
    /// @param x the trace argument
    /// @param s the scalar factor
    virtual Rti& add (const Rti& x, const t_real s);

    /// add a trace with another one
    /// @param x the trace argument
    /// @param y the trace argument
    virtual Rti& add (const Rti& x, const Rti& y);

    /// add a trace with another scaled one
    /// @param x the trace argument
    /// @param y the trace argument
    /// @param s the scalar factor
    virtual Rti& add (const Rti& x, const Rti& y, const t_real s);

    /// substract a trace with a scalar
    /// @param x the trace argument
    /// @param s the scalar factor
    virtual Rti& sub (const Rti& x, const t_real s);

    /// substract a trace with another one
    /// @param x the trace argument
    /// @param y the trace argument
    virtual Rti& sub (const Rti& x, const Rti& y);

    /// multiply a trace with a scaled trace
    /// @param x the trace to multiply
    /// @param s the scaling factor
    virtual Rti& mul (const Rti& x, const t_real s);

    /// multiply a trace with another one
    /// @param x the trace argument
    /// @param y the trace argument
    virtual Rti& mul (const Rti& x, const Rti& y);

    /// divide a trace with another one
    /// @param x the trace argument
    /// @param y the trace argument
    virtual Rti& div (const Rti& x, const Rti& y);

    /// add equal with a trace
    /// @param x the trace to add
    virtual Rti& aeq (const Rti& x);

    /// add equal with a scaled trace
    /// @param x the trace to add
    /// @param s the scaling factor
    virtual Rti& aeq (const Rti& x, const t_real s);

    /// rescale equal with a trace
    /// @param x the trace to add
    /// @param s the scaling factor
    virtual Rti& req (const Rti& x, const t_real s);

    /// permutate this trace
    /// @param p the permutation object
    virtual Rti* permutate (const Cpi& p) const;

    /// reverse this trace permutation
    /// @param p the permutation object
    virtual Rti* reverse (const Cpi& p) const;

  public:
    /// no lock - set a trace by position
    /// @param pos the trace position
    /// @param val the value to set
    virtual void nlset (const t_long pos, const t_real val) =0;

    /// no lock - get a trace value by position
    /// @param pos the trace position
    virtual t_real nlget (const t_long pos) const =0;

  public:
    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const override;

    /// operate this object with another object
    /// @param type   the operator type
    /// @param object the operand object
    Object* oper (t_oper type, Object* object) override;

    /// apply this object with a set of arguments and a quark
    /// @param zobj  the current evaluable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Evaluable* zobj, Nameset* nset, const long quark,
                   Vector* argv) override;
  };
}

#endif
