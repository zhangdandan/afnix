// ---------------------------------------------------------------------------
// - Quaternion.hpp                                                          -
// - afnix:mth module - quaternion object class definition                   -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_QUATERNION_HPP
#define  AFNIX_QUATERNION_HPP

#ifndef  AFNIX_RMATRIX3_HPP
#include "Rmatrix3.hpp"
#endif

namespace afnix {
  
  /// The Quaternion class is an object that implements the behavior of
  /// quaternion, that is a 4-tuple which is mostly used for object
  /// rotation. Unlike other rotation object, the quaternion is numerically
  /// stable and does not suffer from the 'gimbal lock' problem. A quaternion
  /// of norm 1 is called a versor.
  /// @author amaury darsch

  class Quaternion : public virtual Serial {
  public:
    /// compute the opposite of the quaternion
    /// @param q the quaternion to oppose
    friend Quaternion operator - (const Quaternion& q);

    /// add two quaternions together
    /// @param qa the first argument to add
    /// @param qb the second argument to add
    friend Quaternion operator + (const Quaternion& qa, const Quaternion& qb);

    /// substract two quaternions together
    /// @param qa the first argument to add
    /// @param qb the second argument to add
    friend Quaternion operator - (const Quaternion& qa, const Quaternion& qb);

    /// multiply two quaternions together
    /// @param qa the first argument to add
    /// @param qb the second argument to add
    friend Quaternion operator * (const Quaternion& qa, const Quaternion& qb);

  protected:
    /// the real part
    t_real d_w;
    /// the imaginary x component
    t_real d_x;
    /// the imaginary y component
    t_real d_y;
    /// the imaginary z component
    t_real d_z;
    
  public:
    /// create a default quaternion
    Quaternion (void);

    /// create a quaternion by components
    /// @param x the x component
    /// @param y the y component
    /// @param z the z component
    /// @param w the w component
    Quaternion (const t_real w, const t_real x, const t_real y, const t_real z);

    /// copy construct this quaternion
    /// @param that the quaternion to copy
    Quaternion (const Quaternion& that);

    /// assign a quaternion to this one
    /// @param that the quaternion to assign
    Quaternion& operator = (const Quaternion& that);

    /// add a quaternion to this one
    /// @param q the argument to add
    /// @return this added quaternion
    Quaternion& operator += (const Quaternion& q);

    /// multiply a quaternion by a scalar
    /// @param s the scalar factor
    Quaternion& operator *= (const t_real s);

    /// divide a quaternion by a scalar
    /// @param s the scalar factor
    Quaternion& operator /= (const t_real s);

    /// @return the class name
    String repr (void) const override;
    
    /// @return a clone of this object
    Object* clone (void) const override;

    /// @return the serial did
    t_word getdid (void) const override;

    /// @return the serial sid
    t_word getsid (void) const override;
    
    /// serialize this object
    /// @param os the output stream
    void wrstream (OutputStream& os) const override;

    /// deserialize this object
    /// @param is the input stream
    void rdstream (InputStream& os) override;

    /// reset this quaternion
    virtual void reset (void);

    /// set the quaternion by vector
    /// @param v the quaternion vector
    virtual Quaternion& set (const Rvector3& v);

    /// set the quaternion by angle and vector
    /// @param a the quaternion angle
    /// @param v the quaternion vector
    virtual Quaternion& set (const t_real a, const t_real v[3]);

    /// set the quaternion by angle and vector
    /// @param a the quaternion angle
    /// @param v the quaternion vector
    virtual Quaternion& set (const t_real a, const Rvector3& v);

    /// set the quaternion at once
    /// @param w the real part
    /// @param x the x vector component
    /// @param y the y vector component
    /// @param z the z vector component
    virtual Quaternion& set (const t_real w,
			     const t_real x, const t_real y, const t_real z);

    /// check for a x-axis rotation
    virtual bool isrx (void) const;
    
    /// check for a y-axis rotation
    virtual bool isry (void) const;
    
    /// check for a z-axis rotation
    virtual bool isrz (void) const;
    
    /// set the rotation x angle
    /// @param a the rotation angle
    virtual Quaternion& setrx (const t_real a);

    /// set the rotation y angle
    /// @param a the rotation angle
    virtual Quaternion& setry (const t_real a);
    
    /// set the rotation z angle
    /// @param a the rotation angle
    virtual Quaternion& setrz (const t_real a);
    
    /// @return the quaternion real part
    virtual t_real getrval (void) const;

    /// @return the quaternion imaginary part
    virtual Rvector3 getival (void) const;

    /// @return the quaternion module
    virtual t_real tomod (void) const;

    /// @return the quaternion argument
    virtual t_real toarg (void) const;

    /// @return the quaternion direction
    virtual Rvector3 todir (void) const;

    /// @return the quaternion matrix
    virtual Rmatrix3 torot (void) const;
    
    /// normalize this quaternion
    virtual Quaternion& normalize (void);

    /// conjugate this quaternion
    virtual Quaternion& conjugate (void);

    /// rotate a vector with this quaternion
    /// @param v the vector to rotate
    virtual Rvector3 rotate (const Rvector3& v) const;
    
  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);

    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const override;

    /// apply this object with a set of arguments and a quark
    /// @param zobj  the current evaluable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Evaluable* zobj, Nameset* nset, const long quark,
                   Vector* argv) override;
  };
}

#endif
