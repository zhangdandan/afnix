# ---------------------------------------------------------------------------
# - MTH0105.als                                                             -
# - afnix:mth module test unit                                              -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2023 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   trace test unit
# @author amaury darsch

# get the module
interp:library "afnix-mth"

# create a trace 1 object
trans t1 (afnix:mth:Rtrace1)
assert true (afnix:mth:r-trace-1-p t1)
assert "Rtrace1" (t1:repr)
# create a new trace and check
trans t1 (afnix:mth:Rtrace1 1.0)
assert 1.0 (t1:get-x)

# create a trace 2 object
trans t2 (afnix:mth:Rtrace2)
assert true (afnix:mth:r-trace-2-p t2)
assert "Rtrace2" (t2:repr)
# create a new trace and check
trans t2 (afnix:mth:Rtrace2 1.0 2.0)
assert 1.0 (t2:get-x)
assert 2.0 (t2:get-y)

# create a trace 3 object
trans t3 (afnix:mth:Rtrace3)
assert true (afnix:mth:r-trace-3-p t3)
assert "Rtrace3" (t3:repr)
# create a new trace and check
trans t3 (afnix:mth:Rtrace3 1.0 2.0 3.0)
assert 1.0 (t3:get-x)
assert 2.0 (t3:get-y)
assert 3.0 (t3:get-z)

# create a trace 4 object
trans t4 (afnix:mth:Rtrace4)
assert true (afnix:mth:r-trace-4-p t4)
assert "Rtrace4" (t4:repr)
# create a new trace and check
trans t4 (afnix:mth:Rtrace4 1.0 2.0 3.0 0.0)
assert 1.0 (t4:get-x)
assert 2.0 (t4:get-y)
assert 3.0 (t4:get-z)
assert 0.0 (t4:get-w)
