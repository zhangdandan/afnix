# ---------------------------------------------------------------------------
# - MTH0118.als                                                             -
# - afnix:mth module test unit                                              -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2023 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   complex block matrix test unit
# @author amaury darsch

# get the module
interp:library "afnix-mth"

# create a simple square matrix
const cm (afnix:mth:Cblock 2)

# check predicate
assert true (afnix:mth:cmi-p cm)
assert true (afnix:mth:c-block-p cm)

# check representation
assert "Cblock" (cm:repr)

# check matrix size and elements
assert 2    (cm:get-row-size)
assert 2    (cm:get-col-size)
assert true (cm:nil-p)

# check accessors
assert 0.0+0.0i (cm:get 0 0)
cm:set 0 0 1.0
assert 1.0+0.0i (cm:get 0 0)

# check compress and null
assert false (cm:nil-p)
assert false (cm:compress)
assert false (cm:nil-p)

assert 0.0+0.0i (cm:get 0 1)
assert 0.0+0.0i (cm:get 1 0)

assert 0.0+0.0i (cm:get 1 1)
cm:set 1 1 1.0
assert 1.0+0.0i (cm:get 1 1)

# check row and column vectors
trans v (cm:copy-row 0)
assert 1.0+0.0i (v:get 0)
assert 0.0+0.0i (v:get 1)
trans v (cm:copy-row 1)
assert 0.0+0.0i (v:get 0)
assert 1.0+0.0i (v:get 1)

trans v (cm:copy-column 0)
assert 1.0+0.0i (v:get 0)
assert 0.0+0.0i (v:get 1)
trans v (cm:copy-column 1)
assert 0.0+0.0i (v:get 0)
assert 1.0+0.0i (v:get 1)

# create a working vector
const xv (afnix:mth:Cvector 2)
xv:set 0 2.0
xv:set 1 3.0

# test the matrix operators
const bv (* cm xv)
assert true (xv:?= bv)

# nulify and test
cm:set 0 0 0.0
cm:set 1 1 0.0
assert true  (cm:nil-p)
assert false (cm:compress)
assert true  (cm:nil-p)

# create a matrix by vector product
const u (afnix:mth:Cvector 3)
u:set 0 1.0
u:set 1 2.0
u:set 2 3.0
const v (afnix:mth:Cvector 3)
v:set 0 4.0
v:set 1 5.0
v:set 2 6.0

const  m (afnix:mth:Cblock u v)
assert 3 (m:get-row-size)
assert 3 (m:get-col-size)

assert  4.0+0.0i (m:get 0 0)
assert  5.0+0.0i (m:get 0 1)
assert  6.0+0.0i (m:get 0 2)
assert  8.0+0.0i (m:get 1 0)
assert 10.0+0.0i (m:get 1 1)
assert 12.0+0.0i (m:get 1 2)
assert 12.0+0.0i (m:get 2 0)
assert 15.0+0.0i (m:get 2 1)
assert 18.0+0.0i (m:get 2 2)

# create a pecmutation
const p (afnix:mth:Permute 3)
p:set 0 2
p:set 1 0
p:set 2 1

# pecmutate the matrix
const pm (m:permutate p)
assert 12.0+0.0i (pm:get 0 0)
assert 15.0+0.0i (pm:get 0 1)
assert 18.0+0.0i (pm:get 0 2)
assert  4.0+0.0i (pm:get 1 0)
assert  5.0+0.0i (pm:get 1 1)
assert  6.0+0.0i (pm:get 1 2)
assert  8.0+0.0i (pm:get 2 0)
assert 10.0+0.0i (pm:get 2 1)
assert 12.0+0.0i (pm:get 2 2)

# do a reverse permutation
const mp (pm:reverse p)
assert true (m:?= mp)

# create two identity matrices
const mx (afnix:mth:Cblock 2)
const my (afnix:mth:Cblock 2)
mx:eye
my:eye
# create a matrix by tensor product
const mr (afnix:mth:Cblock mx my)
assert 4 (mr:get-row-size)
assert 4 (mr:get-col-size)
assert 1.0+0.0i (mr:get 0 0)
assert 0.0+0.0i (mr:get 0 1)
assert 0.0+0.0i (mr:get 0 2)
assert 0.0+0.0i (mr:get 0 3)
assert 1.0+0.0i (mr:get 1 1)
assert 0.0+0.0i (mr:get 1 0)
assert 0.0+0.0i (mr:get 1 2)
assert 0.0+0.0i (mr:get 1 3)
assert 1.0+0.0i (mr:get 2 2)
assert 0.0+0.0i (mr:get 2 0)
assert 0.0+0.0i (mr:get 2 1)
assert 0.0+0.0i (mr:get 2 3)
assert 1.0+0.0i (mr:get 3 3)
assert 0.0+0.0i (mr:get 3 0)
assert 0.0+0.0i (mr:get 3 1)
assert 0.0+0.0i (mr:get 3 2)
