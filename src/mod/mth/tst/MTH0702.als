# ---------------------------------------------------------------------------
# - MTH0702.als                                                             -
# - afnix:mth module test unit                                              -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2023 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   numeral trace test unit
# @author amaury darsch

# get the module
interp:library "afnix-mth"

# create a simple trace
const nt (afnix:mth:Ntrace 3)

# check predicate
assert true (afnix:mth:ati-p nt)
assert true (afnix:mth:nti-p nt)
assert true (afnix:mth:n-trace-p nt)
# check representation
assert "Ntrace" (nt:repr)

# check trace length and predicate
assert 3 (nt:get-size)
assert true (nt:nil-p)

# check accessors
trans nz (Numeral 0)
assert 0 (nt:get 0)
nt:set 0 nz
assert nz (nt:get 0)

trans no (Numeral 1)
assert 0 (nt:get 1)
nt:set 1 no
assert no (nt:get 1)

trans nw (Numeral 2)
assert 0 (nt:get 2)
nt:set 2 nw
assert nw (nt:get 2)

