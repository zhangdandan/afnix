// ---------------------------------------------------------------------------
// - Graph.hpp                                                               -
// - standard object module - graph base class definition                    -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_GRAPH_HPP
#define  AFNIX_GRAPH_HPP

#ifndef  AFNIX_EDGE_HPP
#include "Edge.hpp"
#endif

#ifndef  AFNIX_VERTEX_HPP
#include "Vertex.hpp"
#endif

namespace afnix {

  /// The Graph class is the base class used to hold a graph. A graph is
  /// a set of edges and vertices. If the edges used to compose the graph are
  /// hyperedges, then the graph is called a hypergraph.
  /// @author amaury darsch

  class Graph : public Object {
  protected:
    /// the set of vertices
    Set d_vset;
    /// the set of edges
    Set d_eset;

  public:
    /// create an empty graph
    Graph (void) =default;

    /// @return the class name
    String repr (void) const override;

    /// reset the graph
    virtual void reset (void);

    /// clear the graph
    virtual void clear (void);

    /// @return true if a vertex exists in this graph
    virtual bool isvrtx (const long sidx) const;

    /// @return true is an edge exists in this graph
    virtual bool isedge (const long sidx) const;

    /// add a vertex to this graph.
    /// @param vrtx the vertex to add
    virtual void add (Vertex* vrtx);

    /// add an edge to this graph.
    /// @param edge the edge to add
    virtual void add (Edge* edge);

    /// @return the number of vertices
    virtual long getvnum (void) const;

    /// @return the number of edges
    virtual long getenum (void) const;

    /// @return a vertex by index
    virtual Vertex* getvrtx (const long vidx) const;

    /// @return an edge by index
    virtual Edge* getedge (const long eidx) const;

  private:
    // make the copy constructor private
    Graph (const Graph&) =delete;
    // make the assignment operator private
    Graph& operator = (const Graph&) =delete;

  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);

    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const override;

    /// apply this object with a set of arguments and a quark
    /// @param zobj  the current evaluable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Evaluable* zobj, Nameset* nset, const long quark,
		   Vector* argv) override;
  };
}

#endif
