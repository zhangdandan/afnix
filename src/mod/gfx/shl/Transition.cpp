// ---------------------------------------------------------------------------
// - Transition.cpp                                                          -
// - afnix:gfx module - transition class implementation                      -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Gfxsid.hxx"
#include "Vector.hpp"
#include "Integer.hpp"
#include "Evaluable.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"
#include "Transition.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a new transition

  Transition::Transition (void) {
    p_cobj = nullptr;
  }

  // create a transition with a client object

  Transition::Transition (Object* cobj) {
    Object::iref (p_cobj = cobj);
  }

  // destroy this transition

  Transition::~Transition (void) {
    Object::dref (p_cobj);
  }

  // return the transition class name

  String Transition::repr (void) const {
    return "Transition";
  }
  
  // return the serial did

  t_word Transition::getdid (void) const {
    return SRL_DEOD_GFX;
  }

  // return the serial sid

  t_word Transition::getsid (void) const {
    return SRL_TRNT_SID;
  }
  
  // serialize this transition

  void Transition::wrstream (OutputStream& os) const {
    rdlock ();
    try {
      if (p_cobj == nullptr) {
	Serial::wrnilid (os);
      } else {
	auto sobj = dynamic_cast<Serial*>(p_cobj);
	if (sobj == nullptr) {
	  throw Exception ("transition-error", "invalid object to serialize",
			   Object::repr (p_cobj));
	}
	sobj->serialize (os);
      }
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // deserialize this transition

  void Transition::rdstream (InputStream& is) {
    wrlock ();
    try {
      Object::dref (p_cobj);
      Object::iref (p_cobj = Serial::deserialize (is));
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // compute the next state transition index

  long Transition::nxsidx (Global* glob, State* stte) const {
    rdlock ();
    try {
      if (stte == nullptr) {
	throw Exception ("transition-error", "null state with next state");
      }
      long sidx = stte->getsidx ();
      if (sidx < 0) {
	throw Exception ("transition-error", "floating state with next state");
      }
      long result = sidx + 1L;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // set the transition object

  void Transition::setcobj (Object* cobj) {
    wrlock ();
    try {
      Object::iref (cobj); Object::dref (p_cobj); p_cobj = cobj;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the transition object

  Object* Transition::getcobj (void) const {
    rdlock ();
    try {
      Object* result = p_cobj;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 3;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the transition supported quarks
  static const long QUARK_NEXT    = zone.intern ("next");
  static const long QUARK_GETCOBJ = zone.intern ("get-client");
  static const long QUARK_SETCOBJ = zone.intern ("set-client");

  // create a new object in a generic way

  Object* Transition::mknew (Vector* argv) {
    long argc = (argv == nullptr) ? 0 : argv->length ();

    // check for 0 argument
    if (argc == 0) return new Transition;
    // check for 1 argument
    if (argc == 1) return new Transition (argv->get (0));
    // invalid arguments
    throw Exception ("argument-error", "too many arguments with transition");
  }

  // return true if the given quark is defined

  bool Transition::isquark (const long quark, const bool hflg) const {
    rdlock ();
    try {
      if (zone.exists (quark) == true) {
	unlock ();
	return true;
      }
      bool result = hflg ? Serial::isquark (quark, hflg) : false;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // apply this object with a set of arguments and a quark

  Object* Transition::apply (Evaluable* zobj, Nameset* nset, const long quark,
			 Vector* argv) {
    // get the number of arguments
    long argc = (argv == nullptr) ? 0 : argv->length ();
    
    // dispatch 0 argument
    if (argc == 0) {
      if (quark == QUARK_GETCOBJ) {
	rdlock ();
	try {
	  Object* result = getcobj ();
	  zobj->post (result);
	  unlock ();
	  return result;
	} catch (...) {
	  unlock ();
	  throw;
	}
      }
    }
    // dispatch 1 arguments
    if (argc == 1) {
      if (quark == QUARK_SETCOBJ) {
	Object* result = argv->get (0);
	setcobj (result);
	return result;
      }
    }
    // dispatch 2 arguments
    if (argc == 2) {
      if (quark == QUARK_NEXT) {
	// collect object
	Object* obj = argv->get (0);
	// check for global
	auto glob = dynamic_cast<Global*>(obj);
	if (glob == nullptr) {
	  throw Exception ("type-error", "invalid object as global in next",
			   Object::repr (obj));
	}
	// check for state
	obj = argv->get (1);
	auto stte = dynamic_cast<State*>(obj);
	if (stte == nullptr) {
	  throw Exception ("type-error", "invalid object as state in next",
			   Object::repr (obj));
	}
	return new Integer (nxsidx (glob, stte));
      }
    }
    // call the serial method
    return Serial::apply (zobj, nset, quark, argv);
  }
}
