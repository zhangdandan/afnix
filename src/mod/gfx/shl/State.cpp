// ---------------------------------------------------------------------------
// - State.cpp                                                               -
// - afnix:gfx module - state class implementation                           -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "State.hpp"
#include "Gfxsid.hxx"
#include "Vector.hpp"
#include "Integer.hpp"
#include "Evaluable.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a new state

  State::State (void) {
    d_mark = false;
    d_sidx = -1L;
    p_cobj = nullptr;
  }

  // create a state with a client object

  State::State (Object* cobj) {
    d_mark = false;
    d_sidx = -1L;
    Object::iref (p_cobj = cobj);
  }

  // destroy this state

  State::~State (void) {
    Object::dref (p_cobj);
  }

  // return the state class name

  String State::repr (void) const {
    return "State";
  }
  
  // return the serial did

  t_word State::getdid (void) const {
    return SRL_DEOD_GFX;
  }

  // return the serial sid

  t_word State::getsid (void) const {
    return SRL_STTE_SID;
  }
  
  // serialize this state

  void State::wrstream (OutputStream& os) const {
    rdlock ();
    try {
      Serial::wrbool (d_mark, os);
      Serial::wrlong (d_sidx, os);
      if (p_cobj == nullptr) {
	Serial::wrnilid (os);
      } else {
	auto sobj = dynamic_cast<Serial*>(p_cobj);
	if (sobj == nullptr) {
	  throw Exception ("state-error", "invalid object to serialize",
			   Object::repr (p_cobj));
	}
	sobj->serialize (os);
      }
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // deserialize this state

  void State::rdstream (InputStream& is) {
    wrlock ();
    try {
      d_mark = Serial::rdbool (is);
      d_sidx = Serial::rdlong (is);
      Object::dref (p_cobj);
      Object::iref (p_cobj = Serial::deserialize (is));
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // clear the state

  void State::clear (void) {
    wrlock ();
    try {
      d_mark = false;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // set the state index

  void State::setsidx (const long sidx) {
    wrlock ();
    try {
      d_sidx = sidx;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the state index

  long State::getsidx (void) const {
    rdlock ();
    try {
      long result = d_sidx;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // set the state object

  void State::setcobj (Object* cobj) {
    wrlock ();
    try {
      Object::iref (cobj); Object::dref (p_cobj); p_cobj = cobj;
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get the state object

  Object* State::getcobj (void) const {
    rdlock ();
    try {
      Object* result = p_cobj;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 5;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the state supported quarks
  static const long QUARK_CLEAR   = zone.intern ("clear");
  static const long QUARK_GETSIDX = zone.intern ("get-index");
  static const long QUARK_SETSIDX = zone.intern ("set-index");
  static const long QUARK_GETCOBJ = zone.intern ("get-client");
  static const long QUARK_SETCOBJ = zone.intern ("set-client");

  // create a new object in a generic way

  Object* State::mknew (Vector* argv) {
    long argc = (argv == nullptr) ? 0 : argv->length ();

    // check for 0 argument
    if (argc == 0) return new State;
    // check for 1 argument
    if (argc == 1) return new State (argv->get (0));
    // invalid arguments
    throw Exception ("argument-error", "too many arguments with state");
  }

  // return true if the given quark is defined

  bool State::isquark (const long quark, const bool hflg) const {
    rdlock ();
    try {
      if (zone.exists (quark) == true) {
	unlock ();
	return true;
      }
      bool result = hflg ? Serial::isquark (quark, hflg) : false;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // apply this object with a set of arguments and a quark

  Object* State::apply (Evaluable* zobj, Nameset* nset, const long quark,
			 Vector* argv) {
    // get the number of arguments
    long argc = (argv == nullptr) ? 0 : argv->length ();
    
    // dispatch 0 argument
    if (argc == 0) {
      if (quark == QUARK_GETSIDX) return new Integer (getsidx ());
      if (quark == QUARK_CLEAR) {
	clear ();
	return nullptr;
      }
      if (quark == QUARK_GETCOBJ) {
	rdlock ();
	try {
	  Object* result = getcobj ();
	  zobj->post (result);
	  unlock ();
	  return result;
	} catch (...) {
	  unlock ();
	  throw;
	}
      }
    }
    // dispatch 1 argument
    if (argc == 1) {
      if (quark == QUARK_SETSIDX) {
	long sidx = argv->getlong (0);
	setsidx (sidx);
	return nullptr;
      }
      if (quark == QUARK_SETCOBJ) {
	Object* result = argv->get (0);
	setcobj (result);
	return result;
      }
    }
    // call the serial method
    return Serial::apply (zobj, nset, quark, argv);
  }
}
