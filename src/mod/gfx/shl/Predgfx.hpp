// ---------------------------------------------------------------------------
// - Predgfx.hpp                                                             -
// - afnix:gfx module - predicates declaration                               -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_PREDGFX_HPP
#define  AFNIX_PREDGFX_HPP

#ifndef  AFNIX_OBJECT_HPP
#include "Object.hpp"
#endif
 
namespace afnix {

  /// this file contains the predicates associated with the afnix:txt
  /// standard module.
  /// @author amaury darsch

  /// the state object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* gfx_sttep (Evaluable* zobj, Nameset* nset, Cons* args);

  /// the transition object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* gfx_trntp (Evaluable* zobj, Nameset* nset, Cons* args);

  /// the edge object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* gfx_edgep (Evaluable* zobj, Nameset* nset, Cons* args);

  /// the vertex object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* gfx_vrtxp (Evaluable* zobj, Nameset* nset, Cons* args);

  /// the graph object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* gfx_grafp (Evaluable* zobj, Nameset* nset, Cons* args);

  /// the automaton object predicate
  /// @param zobj the current evaluable
  /// @param nset the current nameset
  /// @param args the arguments list
  Object* gfx_atmtp (Evaluable* zobj, Nameset* nset, Cons* args);
}

#endif
