// ---------------------------------------------------------------------------
// - Global.cpp                                                          -
// - afnix:gfx module - global class implementation                      -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Gfxsid.hxx"
#include "Vector.hpp"
#include "Evaluable.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"
#include "Global.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // copy construct this global

  Global::Global (const Global& that) {
    that.rdlock ();
    try {
      Plist::operator = (that);
      that.unlock ();
    } catch (...) {
      that.unlock ();
      throw;
    }
  }

  // assign a  global to this one

  Global& Global::operator = (const Global& that) {
    // check for self-assignation
    if (this == &that) return *this;
    // lock and assign
    wrlock ();
    that.rdlock ();
    try {
      Plist::operator = (that);
      unlock ();
      that.unlock ();
      return *this;
    } catch (...) {
      unlock ();
      that.unlock ();
      throw;
    }
  }
  
  // return the global class name

  String Global::repr (void) const {
    return "Global";
  }

  // return a clone of this object
  
  Object* Global::clone (void) const {
    return new Global (*this);
  }

  // return the serial did

  t_word Global::getdid (void) const {
    return SRL_DEOD_GFX;
  }

  // return the serial sid

  t_word Global::getsid (void) const {
    return SRL_GLOB_SID;
  }
  
  // serialize this global

  void Global::wrstream (OutputStream& os) const {
    rdlock ();
    try {
      Plist::wrstream (os);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // deserialize this global

  void Global::rdstream (InputStream& is) {
    wrlock ();
    try {
      Plist::rdstream (is);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // create a new object in a generic way

  Object* Global::mknew (Vector* argv) {
    long argc = (argv == nullptr) ? 0 : argv->length ();

    // check for 0 argument
    if (argc == 0) return new Global;
    // invalid arguments
    throw Exception ("argument-error", "too many arguments with global");
  }

  // return true if the given quark is defined

  bool Global::isquark (const long quark, const bool hflg) const {
    rdlock ();
    try {
      bool result = hflg ? Plist::isquark (quark, hflg) : false;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // apply this object with a set of arguments and a quark

  Object* Global::apply (Evaluable* zobj, Nameset* nset, const long quark,
			 Vector* argv) {
    // call the plist method
    return Serial::apply (zobj, nset, quark, argv);
  }
}
