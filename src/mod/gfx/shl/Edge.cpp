// ---------------------------------------------------------------------------
// - Edge.cpp                                                                -
// - afnix:gfx module - graph edge class implementation                      -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Edge.hpp"
#include "Vertex.hpp"
#include "Vector.hpp"
#include "Integer.hpp"
#include "Evaluable.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a edge with a client object

  Edge::Edge (Object* cobj) {
    setcobj (cobj);
  }

  // destroy this edge

  Edge::~Edge (void) {
    release ();
  }
  
  // return the edge class name

  String Edge::repr (void) const {
    return "Edge";
  }

  // release this object

  void Edge::release (void) {
    wrlock ();
    try {
      Object::iref (this);
      d_vset.reset ();
      Object::tref (this);
      unlock ();
    } catch (...) {
      Object::tref (this);
      unlock ();
      throw;
    }
  }

  // return the edge cardinality

  long Edge::cardinality (void) const {
    rdlock ();
    try {
      long result = d_vset.length ();
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // add an vertex to this edge

  void Edge::add (Vertex* vrtx) {
    wrlock ();
    try {
      if (vrtx != nullptr) d_vset.add (vrtx);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get an vertex by index

  Vertex* Edge::get (const long index) const {
    rdlock ();
    try {
      auto result = dynamic_cast <Vertex*> (d_vset.get (index));
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 3;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the edge supported quarks
  static const long QUARK_ADD     = zone.intern ("add");
  static const long QUARK_GET     = zone.intern ("get");
  static const long QUARK_CARD    = zone.intern ("cardinality");

  // create a new object in a generic way

  Object* Edge::mknew (Vector* argv) {
    long argc = (argv == nullptr) ? 0 : argv->length ();

    // check for 0 argument
    if (argc == 0) return new Edge;
    // check for 1 argument
    if (argc == 1) return new Edge (argv->get (0));
    // invalid arguments
    throw Exception ("argument-error", "too many arguments with edge");
  }

  // return true if the given quark is defined

  bool Edge::isquark (const long quark, const bool hflg) const {
    rdlock ();
    try {
      if (zone.exists (quark) == true) {
	unlock ();
	return true;
      }
      bool result = hflg ? Collectable::isquark (quark, hflg) : false;
      if (result == false) {
	result = hflg ? State::isquark (quark, hflg) : false;
      }
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // apply this object with a set of arguments and a quark

  Object* Edge::apply (Evaluable* zobj, Nameset* nset, const long quark,
			 Vector* argv) {
    // get the number of arguments
    long argc = (argv == nullptr) ? 0 : argv->length ();
    
    // dispatch 0 argument
    if (argc == 0) {
      if (quark == QUARK_CARD) return new Integer (cardinality ());
    }
    // dispatch 1 argument
    if (argc == 1) {
      if (quark == QUARK_ADD) {
	Object* obj = argv->get (0);
	auto vrtx = dynamic_cast <Vertex*> (obj);
	if (vrtx == nullptr) {
	  throw Exception ("type-error", "invalid object as vertex",
			   Object::repr (obj));
	}
	add (vrtx);
	return vrtx;
      }
      if (quark == QUARK_GET) {
	long index = argv->getlong (0);
	rdlock ();
	try {
	  Vertex* result = get (index);
	  zobj->post (result);
	  unlock ();
	  return result;
	} catch (...) {
	  unlock ();
	  throw;
	}
      }
    }
    // check the collectable method
    if (Collectable::isquark (quark, true) == true) {
      return Collectable::apply (zobj, nset, quark, argv);
    }
    // call the state method
    return State::apply (zobj, nset, quark, argv);
  }
}
