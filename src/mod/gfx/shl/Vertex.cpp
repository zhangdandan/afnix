// ---------------------------------------------------------------------------
// - Vertex.cpp                                                              -
// - afnix:gfx module - graph vertex class implementation                    -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Edge.hpp"
#include "Vertex.hpp"
#include "Vector.hpp"
#include "Integer.hpp"
#include "Evaluable.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"

namespace afnix {

  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a vertex with a client object

  Vertex::Vertex (Object* cobj) {
    setcobj (cobj);
  }

  // destroy this vertex

  Vertex::~Vertex (void) {
    release ();
  }
  
  // return the vertex class name

  String Vertex::repr (void) const {
    return "Vertex";
  }

  // release this object

  void Vertex::release (void) {
    wrlock ();
    try {
      Object::iref (this);
      d_eset.reset ();
      Object::tref (this);
      unlock ();
    } catch (...) {
      Object::tref (this);
      unlock ();
      throw;
    }
  }

  // get the vertex degree

  long Vertex::degree (void) const {
    rdlock ();
    try {
      long result = d_eset.length ();
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // add an edge to this vertex

  void Vertex::add (Edge* edge) {
    wrlock ();
    try {
      if (edge != nullptr) d_eset.add (edge);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // get an edge by index

  Edge* Vertex::get (const long eidx) const {
    rdlock ();
    try {
      auto result = dynamic_cast <Edge*> (d_eset.get (eidx));
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 3;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the vertex supported quarks
  static const long QUARK_ADD     = zone.intern ("add");
  static const long QUARK_GET     = zone.intern ("get");
  static const long QUARK_DEGREE  = zone.intern ("degree");

  // create a new object in a generic way

  Object* Vertex::mknew (Vector* argv) {
    long argc = (argv == nullptr) ? 0 : argv->length ();

    // check for 0 argument
    if (argc == 0) return new Vertex;
    // check for 1 argument
    if (argc == 1) return new Vertex (argv->get (0));
    // invalid arguments
    throw Exception ("argument-error", "too many arguments with vertex");
  }

  // return true if the given quark is defined

  bool Vertex::isquark (const long quark, const bool hflg) const {
    rdlock ();
    try {
      if (zone.exists (quark) == true) {
	unlock ();
	return true;
      }
      bool result = hflg ? Collectable::isquark (quark, hflg) : false;
      if (result == false) {
	result = hflg ? State::isquark (quark, hflg) : false;
      }
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // apply this object with a set of arguments and a quark

  Object* Vertex::apply (Evaluable* zobj, Nameset* nset, const long quark,
			 Vector* argv) {
    // get the number of arguments
    long argc = (argv == nullptr) ? 0 : argv->length ();
    
    // dispatch 0 argument
    if (argc == 0) {
      if (quark == QUARK_DEGREE) return new Integer (degree ());
    }
    // dispatch 1 argument
    if (argc == 1) {
      if (quark == QUARK_ADD) {
	Object* obj = argv->get (0);
	auto edge = dynamic_cast <Edge*> (obj);
	if (edge == nullptr) {
	  throw Exception ("type-error", "invalid object as edge",
			   Object::repr (obj));
	}
	add (edge);
	return edge;
      }
      if (quark == QUARK_GET) {
	long index = argv->getlong (0);
	rdlock ();
	try {
	  Edge* result = get (index);
	  zobj->post (result);
	  unlock ();
	  return result;
	} catch (...) {
	  unlock ();
	  throw;
	}
      }
    }
    // check the collectable method
    if (Collectable::isquark (quark, true) == true) {
      return Collectable::apply (zobj, nset, quark, argv);
    }
    // call the state method
    return State::apply (zobj, nset, quark, argv);
  }
}
