# ---------------------------------------------------------------------------
# - GFX0004.als                                                             -
# - afnix:gfx module test unit                                              -
# ---------------------------------------------------------------------------
# - This program is free software;  you can redistribute it  and/or  modify -
# - it provided that this copyright notice is kept intact.                  -
# -                                                                         -
# - This program  is  distributed in  the hope  that it will be useful, but -
# - without  any  warranty;  without  even   the   implied    warranty   of -
# - merchantability or fitness for a particular purpose.  In no event shall -
# - the copyright holder be liable for any  direct, indirect, incidental or -
# - special damages arising in any way out of the use of this software.     -
# ---------------------------------------------------------------------------
# - copyright (c) 1999-2023 amaury darsch                                   -
# ---------------------------------------------------------------------------

# @info   automaton test unit
# @author amaury darsch

# get the module
interp:library "afnix-gfx"

# create an automaton
const  atmt (afnix:gfx:Automaton)
assert true (afnix:gfx:automaton-p atmt)
assert "Automaton" (atmt:repr)

# add state in the automaton
atmt:add (afnix:gfx:State (Integer 0))
atmt:add (afnix:gfx:State (Integer 1))
atmt:add (afnix:gfx:State (Integer 2))

# check content
assert 3 (atmt:length)
trans stte (atmt:get-state 0)
trans cobj (stte:get-client)
assert 0 cobj
assert 0 (stte:get-index)
trans stte (atmt:get-state 1)
trans cobj (stte:get-client)
assert 1 cobj
assert 1 (stte:get-index)
trans stte (atmt:get-state 2)
trans cobj (stte:get-client)
assert 2 cobj
assert 2 (stte:get-index)

for (s) (atmt) {
  assert (s:get-client) (s:get-index)
}
