// ---------------------------------------------------------------------------
// - Pkcs.hpp                                                                -
// - afnix:sec module - pkcs signature class definition                      -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_PKCS_HPP
#define  AFNIX_PKCS_HPP

#ifndef  AFNIX_HASHER_HPP
#include "Hasher.hpp"
#endif

#ifndef  AFNIX_SIGNER_HPP
#include "Signer.hpp"
#endif

namespace afnix {

  /// The Pkcs class is an original implementation of the PKCS signature
  /// standard originally defined by RSA Security Inc as PKCS#1 and now a
  /// standard track referenced as RFC3447. This class implements the V1.5
  /// signature scheme defined as RSASSA-PKCS1-V1_5-SIGN.
  /// @author amaury darsch

  class Pkcs : public Signer {
  private:
    /// the message hasher
    Hasher* p_hash;

  public:
    /// create a default pkcs signature
    Pkcs (void);

    /// create a pkcs signature by key
    /// @param key the signature key
    Pkcs (const Key& key);

    /// create a pkcs signature by signature
    /// @param sign the signature
    Pkcs (const Signature& sign);
    
    /// destroy this signature
    ~Pkcs (void);

    /// @return the class name
    String repr (void) const override;

    /// reset this signature
    void reset (void) override;

    /// set the signature key
    /// @param key the signature key to set
    bool setkey (const Key& key) override;

    /// set the hasher by name
    /// @param name the hasher name
    virtual bool sethash (const String& name);

  protected:
    /// process a message by data
    /// @param data the data to process
    /// @param size the data size
    void process (const t_byte* data, const long size) override;
 
    /// process a message with a buffer
    /// @param buf the buffer to process
    void process (Buffer& buf) override;

    /// process a message with an input stream
    /// @param is the input stream to process
    void process (InputStream& is) override;

    /// finish the signature processing
    Signature finish (void) override;

    /// validate the signature processing
    /// @param sgn the signature to validate
    bool validate (const Signature& sgn) override;
    
  private:
    // make the copy constructor private
    Pkcs (const Pkcs&) =delete;
    // make the assignment operator private
    Pkcs& operator = (const Pkcs&) =delete;

  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);
  };
}

#endif
