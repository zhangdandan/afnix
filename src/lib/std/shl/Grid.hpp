// ---------------------------------------------------------------------------
// - Grid.hpp                                                                -
// - standard object library - grid class definition                         -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_GRID_HPP
#define  AFNIX_GRID_HPP

#ifndef  AFNIX_BITS_HPP
#include "Bits.hpp"
#endif

#ifndef  AFNIX_STRING_HPP
#include "String.hpp"
#endif

namespace afnix {
  
  /// The Grid class is a descriptor of a multi-dimensional counter. The grid
  /// can be used to describe a configuration (aka a maximum size) as well as
  /// a position. When used as an iterator object, a grid can be moved to its
  /// next position.
  /// @author amaury darsch

  class Grid : public Object {
  protected:
    /// the dimensions
    t_array<long>* p_dims;
    
  public:
    /// create a default grid
    Grid (void);

    /// create a null grid by size
    /// @param size the grid size
    Grid (const long size);
    
    /// copy construct this grid
    /// @param that the grid to copy
    Grid (const Grid& that);
    
    /// destroy this grid
    ~Grid (void);

    /// assign a grid to this one
    /// @param that the grid to assign
    Grid& operator = (const Grid& that);

    /// compare two grids
    /// @param that the grid to compare
    bool operator == (const Grid& that) const;
    
    /// compare two grids
    /// @param that the grid to compare
    bool operator < (const Grid& that) const;

    /// compare two grids
    /// @param that the grid to compare
    bool operator <= (const Grid& that) const;
    
    /// @return the class name
    String repr (void) const override;

    /// @return a clone of this object
    Object* clone (void) const override;

    /// @return the grid length
    virtual long length (void) const;

    /// @return the grid dimensions
    virtual long getdims (void) const;

    /// get a dimension by index
    /// @param didx the dimension index
    virtual long getdims (const long didx) const;

    /// create a new grid at the beginning
    virtual Grid begin (void) const;

    /// compute the next grid position
    /// @param grid the current grid
    virtual Grid next (const Grid& grid) const;
    
  public:
    /// create a object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);

    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const override;

    /// apply this object with a set of arguments and a quark
    /// @param zobj  the current evaluable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments to apply
    Object* apply (Evaluable* zobj, Nameset* nset, const long quark,
                   Vector* argv) override;
  };
}

#endif
