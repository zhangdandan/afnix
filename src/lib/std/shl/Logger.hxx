// ---------------------------------------------------------------------------
// - Logger.hxx                                                              -
// - standard object library - private logger definitions                    -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef LOGGER_HXX
#define LOGGER_HXX

#include "Item.hpp"
#include "Logger.hpp"
#include "Exception.hpp"

namespace afnix {

  // the object eval quarks
  static const long QUARK_LOGGER   = String::intern ("Logger");
  static const long QUARK_MLVLFATL = String::intern ("FATAL");
  static const long QUARK_MLVLXERR = String::intern ("ERROR");
  static const long QUARK_MLVLWRNG = String::intern ("WARNING");
  static const long QUARK_MLVLINFO = String::intern ("INFO");
  static const long QUARK_MLVLDBUG = String::intern ("DEBUG");

  // map an item to a message level type
  static inline Logger::t_mlvl item_to_mlvl (const Item& item) {
    // check for a key item
    if (item.gettid () != QUARK_LOGGER)
      throw Exception ("item-error", "item is not a logger item");
    // map the item to the enumeration
    long quark = item.getquark ();
    if (quark == QUARK_MLVLFATL) return Logger::MLVL_FATL;
    if (quark == QUARK_MLVLXERR) return Logger::MLVL_XERR;
    if (quark == QUARK_MLVLWRNG) return Logger::MLVL_WRNG;
    if (quark == QUARK_MLVLINFO) return Logger::MLVL_INFO;
    if (quark == QUARK_MLVLDBUG) return Logger::MLVL_DBUG;
    throw Exception ("item-error", "cannot map item to message level type");
  }
  
  // map a key type to an item
  static inline Item* mlvl_to_item (const Logger::t_mlvl type) {
    switch (type) {
    case Logger::MLVL_FATL:
      return new Item (QUARK_LOGGER, QUARK_MLVLFATL);
      break;
    case Logger::MLVL_XERR:
      return new Item (QUARK_LOGGER, QUARK_MLVLXERR);
      break;
    case Logger::MLVL_WRNG:
      return new Item (QUARK_LOGGER, QUARK_MLVLWRNG);
      break;
    case Logger::MLVL_INFO:
      return new Item (QUARK_LOGGER, QUARK_MLVLINFO);
      break;
    case Logger::MLVL_DBUG:
      return new Item (QUARK_LOGGER, QUARK_MLVLDBUG);
      break;
    }
    return nullptr;
  }  
}

#endif
