// ---------------------------------------------------------------------------
// - Queue.hpp                                                               -
// - standard object library - queue class definition                        -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#ifndef  AFNIX_QUEUE_HPP
#define  AFNIX_QUEUE_HPP

#ifndef  AFNIX_BITS_HPP
#include "Bits.hpp"
#endif

#ifndef  AFNIX_CONDVAR_HPP
#include "Condvar.hpp"
#endif

namespace afnix {

  /// The Queue class is a synchronized fifo class. Unlike the fifo, the
  /// queue is designed to have blocking push and pop methods. The push method
  /// will block if the queue is full, while the pop method will block if the
  /// queue is empty.
  /// @author amaury darsch

  class Queue : public virtual Object {
  private:
    /// the object fifo
    t_fifo<Object*>* p_fifo;
    /// the condition variable
    Condvar d_cvar;
    
  public:
    /// create a default queue
    Queue (void);
  
    /// create a queue by size
    /// @param size the queue size
    Queue (const long size);

    /// copy construct this queue
    /// @param that the object to copy
    Queue (const Queue& that);

    /// destroy this queue
    ~Queue (void);

    /// return the class name
    String repr (void) const override;

    /// assign a queue to this one
    /// @param that the object to assign
    Queue& operator = (const Queue& that);

    /// reset this queue
    virtual void reset (void);

    /// @return the queue size
    virtual long getsize (void) const;

    /// @return the queue length
    virtual long length (void) const;

    /// @return true if the queue is empty
    virtual bool empty (void) const;

    /// @return true if the queue is full
    virtual bool full (void) const;

    /// push an object into the queue
    /// @param obj the object to add
    virtual void push (Object* obj);

    /// pop an object from the queue
    virtual Object* pop (void);

    /// get an object by position index
    /// @param index the object index
    virtual Object* get (const long index) const;

    /// resize this queue
    /// @param size the new queue size
    virtual void resize (const long size);

  public:
    /// create a new object in a generic way
    /// @param argv the argument vector
    static Object* mknew (Vector* argv);

    /// @return true if the given quark is defined
    bool isquark (const long quark, const bool hflg) const override;

    /// apply this object with a set of arguments and a quark
    /// @param zobj  the current evaluable
    /// @param nset  the current nameset    
    /// @param quark the quark to apply these arguments
    /// @param argv  the arguments  to apply
    Object* apply (Evaluable* zobj, Nameset* nset, const long quark,
		   Vector* argv) override;
  };
}

#endif
