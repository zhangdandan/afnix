// ---------------------------------------------------------------------------
// - Loggable.cpp                                                            -
// - standard object library - loggable class implementation                 -
// ---------------------------------------------------------------------------
// - This program is free software;  you can redistribute it  and/or  modify -
// - it provided that this copyright notice is kept intact.                  -
// -                                                                         -
// - This program  is  distributed in  the hope  that it will be useful, but -
// - without  any  warranty;  without  even   the   implied    warranty   of -
// - merchantability or fitness for a particular purpose.  In no event shall -
// - the copyright holder be liable for any  direct, indirect, incidental or -
// - special damages arising in any way out of the use of this software.     -
// ---------------------------------------------------------------------------
// - copyright (c) 1999-2023 amaury darsch                                   -
// ---------------------------------------------------------------------------

#include "Vector.hpp"
#include "Boolean.hpp"
#include "Loggable.hpp"
#include "QuarkZone.hpp"
#include "Exception.hpp"

namespace afnix {
  
  // -------------------------------------------------------------------------
  // - class section                                                         -
  // -------------------------------------------------------------------------

  // create a default loggable

  Loggable::Loggable (void) {
    p_logr = nullptr;
  }

  // create a loggable by logger

  Loggable::Loggable (Logger* logr) {
    Object::iref (p_logr = logr);
  }

  // copy construct this loggable

  Loggable::Loggable (const Loggable& that) {
    that.rdlock ();
    try {
      Object::iref (p_logr = that.p_logr);
      that.unlock ();
    } catch (...) {
      that.unlock ();
      throw;
    }
  }
  
  // copy move this loggable

  Loggable::Loggable (Loggable&& that) noexcept {
    that.wrlock ();
    try {
      // copy move base object
      Object::operator = (static_cast<Object&&>(that));
      // copy move locally
      p_logr = that.p_logr; that.p_logr = nullptr;
    } catch (...) {
      p_logr = nullptr;
    }
    that.unlock ();
  }

  // destroy this loggable

  Loggable::~Loggable (void) {
    Object::dref (p_logr);
  }

  // assign a loggable into this one

  Loggable& Loggable::operator = (const Loggable& that) {
    // check for self-assignation
    if (this == &that) return *this;
    // lock and assign
    wrlock ();
    that.rdlock ();
    try {
      // assign locally
      Object::iref (that.p_logr); Object::dref (p_logr); p_logr = that.p_logr;
      // unlock and return
      unlock ();
      that.unlock ();
      return *this;
    } catch (...) {
      unlock ();
      that.unlock ();
      throw;
    }
  }

  // move this loggable into this one

  Loggable& Loggable::operator = (Loggable&& that) noexcept {
    // check for self-assignation
    if (this == &that) return *this;
    // lock and assign
    wrlock ();
    that.wrlock ();
    try {
      // move base object
      Object::operator = (static_cast<Object&&>(that));
      // move locally
      Object::dref (p_logr); p_logr = that.p_logr; that.p_logr = nullptr;
    } catch (...) {
      p_logr = nullptr;
    }
    unlock ();
    that.unlock ();
    return *this;
  }

  // return the class name

  String Loggable::repr (void) const {
    return "Loggable";
  }

  // get a clone of this object

  Object* Loggable::clone (void) const {
    return new Loggable (*this);
  }

  // bind a logger to this loggable

  bool Loggable::bind (Logger* logr) {
    wrlock ();
    try {
      Object::iref (logr); Object::dref (p_logr); p_logr = logr;
      unlock ();
      return true;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // log an info message

  void Loggable::mvlog (const String& mesg) {
    wrlock ();
    try {
      if (p_logr != nullptr) p_logr->add (Logger::MLVL_INFO, mesg);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // log an error message

  void Loggable::evlog (const String& mesg) {
    wrlock ();
    try {
      if (p_logr != nullptr) p_logr->add (Logger::MLVL_XERR, mesg);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // log a debug message

  void Loggable::dvlog (const String& mesg) {
    wrlock ();
    try {
      if (p_logr != nullptr) p_logr->add (Logger::MLVL_DBUG, mesg);
      unlock ();
    } catch (...) {
      unlock ();
      throw;
    }
  }
  
  // -------------------------------------------------------------------------
  // - object section                                                        -
  // -------------------------------------------------------------------------

  // the quark zone
  static const long QUARK_ZONE_LENGTH = 4;
  static QuarkZone  zone (QUARK_ZONE_LENGTH);

  // the object supported quarks
  static const long QUARK_BIND = zone.intern ("bind");
  static const long QUARK_MLOG = zone.intern ("mvlog");
  static const long QUARK_ELOG = zone.intern ("evlog");
  static const long QUARK_DLOG = zone.intern ("dvlog");

  // create a new object in a generic way

  Object* Loggable::mknew (Vector* argv) {
    // get number of arguments
    long argc = (argv == nullptr) ? 0 : argv->length ();
    // check for 0 argument
    if (argc == 0) return new Loggable;
    // check for 1 argument
    if (argc == 1) {
      // collect object
      Object* obj = argv->get (0);
      // check for logger
      auto logr = dynamic_cast<Logger*>(obj);
      if (logr != nullptr) return new Loggable (logr);
      // invalid object
      throw Exception ("type-error", "invalid object for loggable",
		       Object::repr (obj));
    }
    // invalid arguments
    throw Exception ("argument-error", "too many arguments with style");
  }
  
  // return true if the given quark is defined

  bool Loggable::isquark (const long quark, const bool hflg) const {
    rdlock ();
    try {
      if (zone.exists (quark) == true) {
	unlock ();
	return true;
      }
      bool result = hflg ? Object::isquark (quark, hflg) : false;
      unlock ();
      return result;
    } catch (...) {
      unlock ();
      throw;
    }
  }

  // apply this object with a set of arguments and a quark

  Object* Loggable::apply (Evaluable* zobj, Nameset* nset, const long quark,
			   Vector* argv) {
    // get the number of arguments
    long argc = (argv == nullptr) ? 0 : argv->length ();

    // dispatch 1 argument
    if (argc == 1) {
      if (quark == QUARK_BIND) {
	// collect object
	Object* obj = argv->get (0);
	// check for logger
	auto logr = dynamic_cast<Logger*>(obj);
	if (logr != nullptr) return new Boolean (bind (logr));
	// invalid object
	throw Exception ("type-error", "invalid object to bind for loggable",
			 Object::repr (obj));
      }
      if (quark == QUARK_MLOG) {
	String mesg = argv->getstring(0);
	mvlog (mesg);
	return nullptr;
      }
      if (quark == QUARK_ELOG) {
	String mesg = argv->getstring(0);
	evlog (mesg);
	return nullptr;
      }
      if (quark == QUARK_DLOG) {
	String mesg = argv->getstring(0);
	dvlog (mesg);
	return nullptr;
      }
    }
    // call the object method
    return Object::apply (zobj, nset, quark, argv);
  }
}
